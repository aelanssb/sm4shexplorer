﻿using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace UIBuilder
{
    class NutBox : GLControl
    {
        public Nut Nut = null;
        public Vector2 TopLeft = Vector2.Zero;
        public Vector2 BotRight = Vector2.One;

        public void setNut (Nut nut, Vector2 topLeft, Vector2 botRight)
        {
            Nut = nut;
            TopLeft = topLeft;
            BotRight = botRight;
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (DesignMode)
                return;

            Plugin.Context.MakeCurrent(WindowInfo);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.PushAttrib(AttribMask.AllAttribBits);

            GL.MatrixMode(MatrixMode.Projection);
            GL.PushMatrix();
            GL.LoadIdentity();

            GL.Ortho(
                0,
                ClientRectangle.Width,
                ClientRectangle.Height,
                0,
                0, 10
            );

            GL.Viewport(ClientRectangle);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();
            GL.LoadIdentity();

            if (Nut != null)
            {
                GL.BindTexture(TextureTarget.Texture2D, Nut.glId);
                GL.Begin(PrimitiveType.Quads);
                GL.TexCoord2(TopLeft.X, TopLeft.Y);
                GL.Vertex2(0, 0);
                GL.TexCoord2(BotRight.X, TopLeft.Y);
                GL.Vertex2(ClientRectangle.Width, 0);
                GL.TexCoord2(BotRight.X, BotRight.Y);
                GL.Vertex2(ClientRectangle.Width, ClientRectangle.Height);
                GL.TexCoord2(TopLeft.X, BotRight.Y);
                GL.Vertex2(0, ClientRectangle.Height);
                GL.End();
                GL.BindTexture(TextureTarget.Texture2D, 0);
            }

            GL.PopMatrix();
            GL.MatrixMode(MatrixMode.Projection);
            GL.PopMatrix();
            GL.MatrixMode(MatrixMode.Modelview);
            GL.PopAttrib();

            SwapBuffers();
        }
    }
}
