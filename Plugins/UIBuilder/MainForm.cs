﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using OpenTK.Graphics;
using WeifenLuo.WinFormsUI.Docking;

namespace UIBuilder
{
    public partial class MainForm : Form
    {
        private List<BaseWindow> editors = new List<BaseWindow>();
        private BaseWindow activeEditor = null;
        private ToolStrip activeToolbar = null;

        private DocumentOutlinePanel documentOutlinePanel;
        private TexturePanel texturePanel;
        private FiletreePanel filetreePanel;

        static private MainForm instance;
        static public BaseWindow ActiveEditor => instance.activeEditor;
        static public ToolStripMenuItem MenuFileSave => instance.saveToolStripMenuItem;
        static public ToolStripMenuItem MenuEditUndo => instance.undoToolStripMenuItem;
        static public ToolStripMenuItem MenuEditRedo => instance.redoToolStripMenuItem;
        static public ToolStripMenuItem MenuViewShowMiis => instance.showMiisToolStripMenuItem;
        static public ToolStripMenuItem MenuViewDrawIconBackground => instance.drawIconBackgroundToolStripMenuItem;

        public MainForm (string filename)
        {
            InitializeComponent();
            activeToolbar = toolStrip1;

            var windowInfo = OpenTK.Platform.Utilities.CreateWindowsWindowInfo(Handle);
            Plugin.Context = new GraphicsContext(GraphicsMode.Default, windowInfo);
            MainForm.instance = this;

            dockPanel.Theme = new VS2015BlueTheme();

            //if (filename != null)
            //    AddEditor(filename);

            debugToolStripMenuItem_Click(null, null);

            #if DEBUG
            //if (filename == null)
            //    debugToolStripMenuItem_Click(null, null);

            var debugItem = new ToolStripButton();
            debugItem.Text = "Debug";
            debugItem.Click += debugToolStripMenuItem_Click;
            menuStrip1.Items.Add(debugItem);
            #endif

            //texturePanel = new TexturePanel();
            //texturePanel.Show(dockPanel, DockState.DockRightAutoHide);
            filetreePanel = new FiletreePanel();
            filetreePanel.Show(dockPanel, DockState.DockLeftAutoHide);
        }

        ~MainForm()
        {
            MainForm.instance = null;
        }

        public static void AddEditor (string filename)
        {
            BaseWindow editor = null;

            if (filename.EndsWith("chara.lm"))
                editor = new CSSEditorWindow(filename);
            else if (filename.EndsWith("stage.lm"))
                editor = new SSSEditorWindow(filename);
            else if (filename.EndsWith(".lm"))
                editor = new LumenEditorWindow(filename);

            else if (filename.EndsWith(".nut"))
                editor = new TextureEditorWindow(filename);

            if (editor != null)
            {
                editor.Show(instance.dockPanel, DockState.Document);
                editor.FormClosed += OnEditorClosed;
                instance.editors.Add(editor);
            }
        }

        static void OnEditorClosed(object sender, FormClosedEventArgs e)
        {
            instance.editors.Remove((BaseWindow)sender);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var z = editors.ToArray();
            foreach (var editor in z)
            {
                editor.Close();
            }
        }

        private void dockPanel_ActiveDocumentChanged(object sender, EventArgs e)
        {
            if (activeEditor != null)
                activeEditor.OnFocusLost();

            if (activeToolbar != null)
                Controls.Remove(activeToolbar);

            activeEditor = dockPanel.ActiveDocument as BaseWindow;
            if (activeEditor != null)
            {
                activeEditor.OnFocusGained();
                if (activeEditor.Toolbar != null)
                {
                    activeToolbar = activeEditor.Toolbar;
                    Controls.Add(activeToolbar);
                    activeToolbar.BringToFront();
                }
            }

            if (activeEditor == null && activeToolbar == null)
            {
                activeToolbar = toolStrip1;
                Controls.Add(activeToolbar);
                activeToolbar.BringToFront();
            }
        }

        #region Menu Events
        private void texturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (texturePanel == null || texturePanel.IsDisposed)
                texturePanel = new TexturePanel();

            texturePanel.Show(dockPanel, DockState.DockRight);
        }

        private void debugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //var editor = new DebugWindow();
            //editor.Show(instance.dockPanel, DockState.Document);
            //editor.FormClosed += OnEditorClosed;
            //instance.editors.Add(editor);
        }

        private void fileTreeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (filetreePanel.IsDisposed)
                filetreePanel = new FiletreePanel();

            filetreePanel.Show(dockPanel, DockState.DockLeft);
        }

        private void documentOutlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (documentOutlinePanel == null || documentOutlinePanel.IsDisposed)
                documentOutlinePanel = new DocumentOutlinePanel();

            documentOutlinePanel.Show(dockPanel, DockState.DockRight);
        }

        private void openToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var ofd = new OpenFileDialog();
            var result = ofd.ShowDialog();

            if (result == DialogResult.OK)
                AddEditor(ofd.FileName);
        }

        private void saveToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (activeEditor != null)
                activeEditor.Save();
        }

        private void closeToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (activeEditor != null)
                activeEditor.Close();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (activeEditor != null)
                activeEditor.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (activeEditor != null)
                activeEditor.Redo();
        }
        #endregion
    }
}
