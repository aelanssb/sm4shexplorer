﻿using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace UIBuilder
{
    public partial class DocumentOutlinePanel : DockContent
    {
        public DocumentOutlinePanel()
        {
            InitializeComponent();

            if (MainForm.ActiveEditor is LumenEditorWindow)
            {
                var document = (MainForm.ActiveEditor as LumenEditorWindow).Document;

                addBranch(document.Root, tree.Nodes);
                tree.Nodes[0].Expand();
            }
        }

        private void addBranch (MovieClipInstance parentMc, TreeNodeCollection parentNode)
        {
            var node = new TreeNode();
            node.Text = parentMc.Name;
            node.ImageIndex = 2;
            node.SelectedImageIndex = 2;
            node.Checked = true;
            node.Tag = parentMc;
            parentNode.Add(node);

            foreach (var kv in parentMc.Nodes)
            {
                if (kv.Value is MovieClipInstance)
                    addBranch(kv.Value as MovieClipInstance, node.Nodes);
                else
                {
                    var n = new TreeNode();
                    n.Checked = true;
                    n.Tag = kv.Value;

                    if (kv.Value is DynamicTextInstance)
                    {
                        n.ImageIndex = 1;
                        n.SelectedImageIndex = 1;
                        n.Text = (kv.Value as DynamicTextInstance).Name;
                    }
                    else
                    {
                        n.Text = "SHAPE";
                    }

                    node.Nodes.Add(n);
                }
            }
        }

        private void tree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (!(MainForm.ActiveEditor is LumenEditorWindow))
                return;
        }

        private void tree_AfterCheck(object sender, TreeViewEventArgs e)
        {
            var obj = (LumenObjectInstance)e.Node.Tag;
            obj.Draw = e.Node.Checked;
        }
    }
}
