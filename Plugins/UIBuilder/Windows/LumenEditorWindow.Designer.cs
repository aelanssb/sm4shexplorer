﻿namespace UIBuilder
{
    partial class LumenEditorWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LumenEditorWindow));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusbarMousePosition = new System.Windows.Forms.ToolStripStatusLabel();
            this.glControl = new OpenTK.GLControl();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolbarAlignLeft = new System.Windows.Forms.ToolStripButton();
            this.toolbarAlignRight = new System.Windows.Forms.ToolStripButton();
            this.toolbarAlignTop = new System.Windows.Forms.ToolStripButton();
            this.toolbarAlignBottom = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbarAlignHorizontalCenter = new System.Windows.Forms.ToolStripButton();
            this.toolbarAlignVerticalCenter = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbarDistributeHorizonal = new System.Windows.Forms.ToolStripButton();
            this.toolbarDistributeVertical = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbarSwap = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusbarMousePosition});
            this.statusStrip1.Location = new System.Drawing.Point(0, 239);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(284, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusbarMousePosition
            // 
            this.statusbarMousePosition.AutoSize = false;
            this.statusbarMousePosition.Name = "statusbarMousePosition";
            this.statusbarMousePosition.Size = new System.Drawing.Size(150, 17);
            this.statusbarMousePosition.Text = "0, 0";
            // 
            // glControl
            // 
            this.glControl.BackColor = System.Drawing.Color.Black;
            this.glControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glControl.Location = new System.Drawing.Point(0, 0);
            this.glControl.Name = "glControl";
            this.glControl.Size = new System.Drawing.Size(284, 261);
            this.glControl.TabIndex = 3;
            this.glControl.VSync = true;
            this.glControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl_MouseDown);
            this.glControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl_MouseMove);
            this.glControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl_MouseUp);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolbarAlignLeft,
            this.toolbarAlignRight,
            this.toolbarAlignTop,
            this.toolbarAlignBottom,
            this.toolStripSeparator5,
            this.toolbarAlignHorizontalCenter,
            this.toolbarAlignVerticalCenter,
            this.toolStripSeparator6,
            this.toolbarDistributeHorizonal,
            this.toolbarDistributeVertical,
            this.toolStripSeparator7,
            this.toolbarSwap});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(284, 25);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolbarAlignLeft
            // 
            this.toolbarAlignLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarAlignLeft.Image = global::UIBuilder.Properties.Resources.icon_align_left;
            this.toolbarAlignLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarAlignLeft.Name = "toolbarAlignLeft";
            this.toolbarAlignLeft.Size = new System.Drawing.Size(23, 22);
            this.toolbarAlignLeft.Text = "Align Left";
            // 
            // toolbarAlignRight
            // 
            this.toolbarAlignRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarAlignRight.Image = global::UIBuilder.Properties.Resources.icon_align_right;
            this.toolbarAlignRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarAlignRight.Name = "toolbarAlignRight";
            this.toolbarAlignRight.Size = new System.Drawing.Size(23, 22);
            this.toolbarAlignRight.Text = "Align Right";
            // 
            // toolbarAlignTop
            // 
            this.toolbarAlignTop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarAlignTop.Image = global::UIBuilder.Properties.Resources.icon_align_top;
            this.toolbarAlignTop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarAlignTop.Name = "toolbarAlignTop";
            this.toolbarAlignTop.Size = new System.Drawing.Size(23, 22);
            this.toolbarAlignTop.Text = "Align Top";
            // 
            // toolbarAlignBottom
            // 
            this.toolbarAlignBottom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarAlignBottom.Image = global::UIBuilder.Properties.Resources.icon_align_bottom;
            this.toolbarAlignBottom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarAlignBottom.Name = "toolbarAlignBottom";
            this.toolbarAlignBottom.Size = new System.Drawing.Size(23, 22);
            this.toolbarAlignBottom.Text = "Align Bottom";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolbarAlignHorizontalCenter
            // 
            this.toolbarAlignHorizontalCenter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarAlignHorizontalCenter.Image = global::UIBuilder.Properties.Resources.icon_align_horizontalcenter;
            this.toolbarAlignHorizontalCenter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarAlignHorizontalCenter.Name = "toolbarAlignHorizontalCenter";
            this.toolbarAlignHorizontalCenter.Size = new System.Drawing.Size(23, 22);
            this.toolbarAlignHorizontalCenter.Text = "Align Horizontal Center";
            // 
            // toolbarAlignVerticalCenter
            // 
            this.toolbarAlignVerticalCenter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarAlignVerticalCenter.Image = global::UIBuilder.Properties.Resources.icon_align_verticalcenter;
            this.toolbarAlignVerticalCenter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarAlignVerticalCenter.Name = "toolbarAlignVerticalCenter";
            this.toolbarAlignVerticalCenter.Size = new System.Drawing.Size(23, 22);
            this.toolbarAlignVerticalCenter.Text = "Align Vertical Center";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolbarDistributeHorizonal
            // 
            this.toolbarDistributeHorizonal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarDistributeHorizonal.Image = global::UIBuilder.Properties.Resources.icon_distribute_horizontal;
            this.toolbarDistributeHorizonal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarDistributeHorizonal.Name = "toolbarDistributeHorizonal";
            this.toolbarDistributeHorizonal.Size = new System.Drawing.Size(23, 22);
            this.toolbarDistributeHorizonal.Text = "Distribute Horizontal";
            // 
            // toolbarDistributeVertical
            // 
            this.toolbarDistributeVertical.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarDistributeVertical.Image = global::UIBuilder.Properties.Resources.icon_distribute_vertical;
            this.toolbarDistributeVertical.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarDistributeVertical.Name = "toolbarDistributeVertical";
            this.toolbarDistributeVertical.Size = new System.Drawing.Size(23, 22);
            this.toolbarDistributeVertical.Text = "Distribute Vertical";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // toolbarSwap
            // 
            this.toolbarSwap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbarSwap.Image = ((System.Drawing.Image)(resources.GetObject("toolbarSwap.Image")));
            this.toolbarSwap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbarSwap.Name = "toolbarSwap";
            this.toolbarSwap.Size = new System.Drawing.Size(23, 22);
            this.toolbarSwap.Text = "Swap Icons";
            this.toolbarSwap.ToolTipText = "Swap Icons";
            // 
            // LumenEditorWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.glControl);
            this.Name = "LumenEditorWindow";
            this.Text = "LumenEditorWindow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SSSEditorWindow_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusbarMousePosition;
        private OpenTK.GLControl glControl;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolbarAlignLeft;
        private System.Windows.Forms.ToolStripButton toolbarAlignRight;
        private System.Windows.Forms.ToolStripButton toolbarAlignTop;
        private System.Windows.Forms.ToolStripButton toolbarAlignBottom;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolbarAlignHorizontalCenter;
        private System.Windows.Forms.ToolStripButton toolbarAlignVerticalCenter;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolbarDistributeHorizonal;
        private System.Windows.Forms.ToolStripButton toolbarDistributeVertical;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton toolbarSwap;
    }
}