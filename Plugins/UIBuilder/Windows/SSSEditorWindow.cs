﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Collections.Specialized;
using System.Diagnostics;

namespace UIBuilder
{
    public partial class SSSEditorWindow : BaseWindow
    {
        Lumen stage_lm;

        bool dirty = false;
        string currentFilename = null;
        ObservableCollection<int> selectedIcons = new ObservableCollection<int>();
        ObservableCollection<UndoState> undoStack = new ObservableCollection<UndoState>();
        List<UndoState> redoStack = new List<UndoState>();
        int currentLayout = 0;
        Vector2 viewPosition = new Vector2();

        bool dragging = false;
        Vector2 dragStartPosition = new Vector2();
        bool shiftHeld = false;
        bool rmbHeld = false;
        bool boxSelecting = false;
        Vector2 boxSelectStartPosition = new Vector2();
        Vector2 mousePosition = new Vector2();

        public ToolStripZoomControl statusbarZoom = new ToolStripZoomControl();
        public ToolStripComboBoxControl comboBoxLayout = new ToolStripComboBoxControl();

        public SSSEditorWindow(string filename)
        {
            InitializeComponent();

            statusStrip1.Items.Add(new ToolStripLabel("Zoom:"));
            statusbarZoom.SelectedValueChanged += statusbarZoom_SelectedValueChanged;
            statusStrip1.Items.Add(statusbarZoom);

            statusStrip1.Items.Add(new ToolStripSeparator());
            statusStrip1.Items.Add(new ToolStripLabel("Layout:"));

            comboBoxLayout.SelectedIndexChanged += comboBoxLayout_SelectedIndexChanged;
            comboBoxLayout.ComboBoxControl.DropDownStyle = ComboBoxStyle.DropDownList;
            statusStrip1.Items.Add(comboBoxLayout);

            glControl.PreviewKeyDown += _PreviewKeyDown;
            glControl.MouseWheel += glControl_MouseWheel;

            undoStack.CollectionChanged += undoStackChanged;
            selectedIcons.CollectionChanged += selectedIconsChanged;

            currentFilename = filename;
            Text = Path.GetFileName(filename);
            TabText = Text;
        }

        protected override bool UseSave => true;
        protected override bool UseUndo => true;
        public override ToolStrip Toolbar => toolStrip1;

        private void selectedIconsChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            toolbarSwap.Enabled = (selectedIcons.Count == 2);

            bool alignEnabled = (selectedIcons.Count >= 2);
            toolbarAlignTop.Enabled = alignEnabled;
            toolbarAlignBottom.Enabled = alignEnabled;
            toolbarAlignLeft.Enabled = alignEnabled;
            toolbarAlignRight.Enabled = alignEnabled;
            toolbarAlignHorizontalCenter.Enabled = alignEnabled;
            toolbarAlignVerticalCenter.Enabled = alignEnabled;

            bool distributeEnabled = (selectedIcons.Count > 2);
            toolbarDistributeHorizonal.Enabled = distributeEnabled;
            toolbarDistributeVertical.Enabled = distributeEnabled;
        }

        private void SSSEditorWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (dirty)
            {
                var res = MessageBox.Show(
                    $"Save changes to {Text}?",
                    "UI Builder",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.None,
                    MessageBoxDefaultButton.Button1
                );

                if (res == DialogResult.Yes)
                {
                    Save();
                }
                else if (res == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }
            }

            Application.Idle -= Application_Idle;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!DesignMode)
            {
                Plugin.Context.MakeCurrent(glControl.WindowInfo);

                stage_lm = new Lumen(currentFilename);

                foreach (var label in stage_lm.movieclips[167].labels)
                {
                    comboBoxLayout.Items.Add(stage_lm.symbols[label.nameId]);
                }
                comboBoxLayout.SelectedIndex = currentLayout;

                GL.ClearColor(Color.Black);
                GL.Enable(EnableCap.Texture2D);
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

                statusbarZoom.Value = (float)glControl.ClientRectangle.Width / 1920.0f;

                //ReplaceTexture("stage_img_rep_01", new Nut(Plugin.GetAsset("data/ui/replace/stage/stage_10/stage_10_BattleField_f.nut")));
                //ReplaceTexture("stage_img_rep_02", new Nut(Plugin.GetAsset("data/ui/replace/stage/stage_10/stage_10_End_f.nut")));

                Application.Idle += Application_Idle;
            }

        }

        Lumen.MovieClip getMCFromId(int id)
        {
            foreach (var mc in stage_lm.movieclips)
            {
                if (mc.id == id)
                    return mc;
            }

            return null;
        }

        Vector2 getSizeFromIconObjectId(int oid)
        {
            return new Vector2(82, 54);
        }

        Lumen.MovieClip MC
        {
            get
            {
                if (currentLayout >= 48)
                    return stage_lm.movieclips[119 + currentLayout];

                return stage_lm.movieclips[67 + currentLayout];
            }
        }

        private void Application_Idle(object sender, EventArgs e)
        {
            if (!glControl.IsIdle)
                return;

            Plugin.Context.MakeCurrent(glControl.WindowInfo);
            GL.PushAttrib(AttribMask.AllAttribBits);

            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Color3(Color.White);

            var ortho = Matrix4.CreateOrthographicOffCenter(
                0,
                glControl.ClientRectangle.Width / statusbarZoom.Value,
                glControl.ClientRectangle.Height / statusbarZoom.Value,
                0,
                0, 10
            );

            GL.MatrixMode(MatrixMode.Projection);
            GL.PushMatrix();
            GL.LoadMatrix(ref ortho);
            GL.Viewport(glControl.ClientRectangle);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();
            GL.LoadIdentity();

            GL.PushMatrix();
            GL.Translate(viewPosition.X, viewPosition.Y, 0);
            GL.PushMatrix();
            translateToPosition(0x1067);

            #region Draw Background
            GL.PushMatrix();
            //GL.BindTexture(TextureTarget.Texture2D, Plugin.TextureAtlases[chara_lm.shapes[0].graphics[0].atlasId].glId);

            GL.Begin(PrimitiveType.Quads);
            foreach (var vert in stage_lm.shapes[0].graphics[0].verts)
            {
                //GL.TexCoord2(vert.uv.X, vert.uv.Y);
                //GL.Vertex2(vert.pos.X, vert.pos.Y);
            }
            GL.End();
            GL.PopMatrix();
            #endregion

            #region Draw img2_group
            var img2_group = stage_lm.movieclips[27].keyframes[0];
            GL.PushMatrix();
            translateToPosition(0x1068);

            GL.PushMatrix();
            translateToPosition(img2_group.placements[0].positionId);
            //stage_lm.objects[img2_group.placements[0].objectId].Draw();
            GL.PopMatrix();

            GL.PushMatrix();
            GL.Color3(0.105882353, 0.105882353, 0.105882353);
            translateToPosition(img2_group.placements[1].positionId);
            //stage_lm.objects[img2_group.placements[1].objectId].Draw();
            GL.Color3(Color.White);
            GL.PopMatrix();

            GL.PushMatrix();
            translateToPosition(img2_group.placements[4].positionId);
            //stage_lm.objects[img2_group.placements[4].objectId].Draw();
            GL.PopMatrix();

            GL.PopMatrix();
            #endregion

            #region Draw Icons
            Vector2 size = getSizeFromIconObjectId(-1);
            GL.PushMatrix();
            translateToPosition(0x103B);

            for (int i = 0; i < MC.frames[0].placements.Count; i++)
            {
                GL.PushMatrix();
                var placement = MC.frames[0].placements[i];
                var pos = stage_lm.positions[placement.positionId];
                GL.Translate(pos.X, pos.Y, 0);

                GL.BindTexture(TextureTarget.Texture2D, 0);

                var p = stage_lm.movieclips[57].frames[i % 47];
                //stage_lm.objects[p.placements[0].objectId]?.Draw();

                GL.Color3(Color.White);
                GL.Begin(PrimitiveType.Quads);
                GL.TexCoord2(0.00390625f, 0.0078125f);
                GL.Vertex2(-size.X, -size.Y);
                GL.TexCoord2(0.996094f, 0.0078125f);
                GL.Vertex2(size.X, -size.Y);
                GL.TexCoord2(0.996094f, 0.992188);
                GL.Vertex2(size.X, size.Y);
                GL.TexCoord2(0.00390625f, 0.992188);
                GL.Vertex2(-size.X, size.Y);
                GL.End();

                if (selectedIcons.Contains(i))
                {
                    GL.BindTexture(TextureTarget.Texture2D, 0);
                    GL.Color3(Color.Red);
                    GL.LineWidth(1);
                    GL.Begin(PrimitiveType.LineLoop);
                    GL.TexCoord2(0.00390625f, 0.0078125f);
                    GL.Vertex2(-size.X, -size.Y);
                    GL.TexCoord2(0.996094f, 0.0078125f);
                    GL.Vertex2(size.X, -size.Y);
                    GL.TexCoord2(0.996094f, 0.992188);
                    GL.Vertex2(size.X, size.Y);
                    GL.TexCoord2(0.00390625f, 0.992188);
                    GL.Vertex2(-size.X, size.Y);
                    GL.End();
                    GL.Color3(Color.White);
                }

                GL.PopMatrix(); // icon position
            }

            GL.PopMatrix();
            #endregion

            if (dragging)
            {
                GL.LineWidth(1);
                foreach (var selected in selectedIcons)
                {
                    GL.PushMatrix();
                    var placement = MC.frames[0].placements[selected];
                    var pos = stage_lm.positions[placement.positionId];
                    translateToPosition(0x103B);
                    GL.Translate(pos.X, pos.Y, 0);

                    var delta = mousePosition - dragStartPosition;
                    delta /= statusbarZoom.Value;
                    GL.Translate(delta.X, delta.Y, 0);

                    GL.Color3(Color.White);
                    GL.Begin(PrimitiveType.LineLoop);
                    GL.Vertex2(-size.X, -size.Y);
                    GL.Vertex2(size.X, -size.Y);
                    GL.Vertex2(size.X, size.Y);
                    GL.Vertex2(-size.X, size.Y);
                    GL.End();
                    GL.PopMatrix();
                }
            }

            GL.PopMatrix(); // icons center
            GL.PopMatrix(); // viewOffset

            if (boxSelecting)
            {
                GL.Color3(Color.Red);

                GL.Scale(1.0 / statusbarZoom.Value, 1.0 / statusbarZoom.Value, 0);
                GL.Begin(PrimitiveType.LineLoop);
                GL.Vertex2(boxSelectStartPosition.X, boxSelectStartPosition.Y);
                GL.Vertex2(mousePosition.X, boxSelectStartPosition.Y);
                GL.Vertex2(mousePosition.X, mousePosition.Y);
                GL.Vertex2(boxSelectStartPosition.X, mousePosition.Y);
                GL.End();
            }

            GL.PopMatrix();
            GL.MatrixMode(MatrixMode.Projection);
            GL.PopMatrix();
            GL.MatrixMode(MatrixMode.Modelview);
            GL.PopAttrib();

            glControl.SwapBuffers();
        }

        void moveSelectedBy(float x, float y)
        {
            if (x == 0 && y == 0)
                return;

            var state = new UndoState();
            if (selectedIcons.Count > 1)
                state.text = $"move {selectedIcons.Count} icons";
            else
                state.text = "move icon";

            foreach (var selected in selectedIcons)
            {
                var positionId = MC.frames[0].placements[selected].positionId;
                var pos = stage_lm.positions[positionId];
                state.positions[positionId] = pos;
                stage_lm.positions[positionId] = new Vector2(pos.X + x, pos.Y + y);
            }

            redoStack.Clear();
            undoStack.Add(state);
        }

        void translateToPosition(int positionId)
        {
            var position = stage_lm.positions[positionId];
            GL.Translate(position.X, position.Y, 0);
        }

        public override void Save()
        {
            dirty = false;
            TabText = Text;

            stage_lm.SaveMetadata();

            using (var stream = new FileStream(currentFilename, FileMode.Create))
            using (var writer = new BinaryWriter(stream))
                writer.Write(stage_lm.Rebuild());
        }

        private void _PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Right:
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Shift | Keys.Right:
                case Keys.Shift | Keys.Left:
                case Keys.Shift | Keys.Up:
                case Keys.Shift | Keys.Down:
                case Keys.ShiftKey:
                    e.IsInputKey = true;
                    break;
            }
        }

        #region Input Events
        private void glControl_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey)
                shiftHeld = false;
        }

        private void glControl_MouseMove(object sender, MouseEventArgs e)
        {
            statusbarMousePosition.Text = $"{e.X / statusbarZoom.Value - viewPosition.X:f0}, {e.Y / statusbarZoom.Value - viewPosition.Y:f0}";
            var newMousePos = new Vector2(e.X, e.Y);

            if (rmbHeld)
                viewPosition += (newMousePos - mousePosition);

            mousePosition = newMousePos;
        }


        private void glControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                rmbHeld = true;
                return;
            }

            // oh god
            Vector2 mp = new Vector2(e.X, e.Y);
            mp -= viewPosition * statusbarZoom.Value;

            Vector2 size = getSizeFromIconObjectId(-1);
            size *= statusbarZoom.Value;

            var position = stage_lm.positions[0x1067] + stage_lm.positions[0x103B];

            // Reverse sort so selection order is same as drawing order.
            for (int i = MC.frames[0].placements.Count - 1; i >= 0; i--)
            {
                var placement = MC.frames[0].placements[i];
                var shapePos = stage_lm.positions[placement.positionId];

                // TODO: the inverse of this shit should be done to the cursor instead...
                RectangleF shape = new RectangleF(
                    (position.X * statusbarZoom.Value + shapePos.X * statusbarZoom.Value - size.X),
                    (position.Y * statusbarZoom.Value + shapePos.Y * statusbarZoom.Value - size.Y),
                    size.X * 2,
                    size.Y * 2
                );

                if (shape.Contains(mp.X, mp.Y))
                {
                    dragging = true;
                    dragStartPosition.X = e.X;
                    dragStartPosition.Y = e.Y;

                    if (selectedIcons.Contains(i))
                        return;

                    if (!shiftHeld)
                        selectedIcons.Clear();

                    selectedIcons.Add(i);

                    return;
                }
            }

            if (!shiftHeld)
                selectedIcons.Clear();

            boxSelecting = true;
            boxSelectStartPosition = new Vector2(e.X, e.Y);
        }


        private void glControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (dragging && selectedIcons.Count > 0)
            {
                var delta = mousePosition - dragStartPosition;
                delta /= statusbarZoom.Value;
                moveSelectedBy(delta.X, delta.Y);
            }

            dragging = false;

            if (e.Button == MouseButtons.Right)
                rmbHeld = false;

            if (boxSelecting)
            {
                boxSelecting = false;
                if (!shiftHeld)
                    selectedIcons.Clear();

                var position = stage_lm.positions[0x1067] + stage_lm.positions[0x103B];
                Vector2 size = getSizeFromIconObjectId(-1);
                size *= statusbarZoom.Value;

                // TODO: apply inverse scale to mouse coord to save computation.

                RectangleF selectionRect = new RectangleF(
                    Math.Min(boxSelectStartPosition.X, e.X) - viewPosition.X * statusbarZoom.Value,
                    Math.Min(boxSelectStartPosition.Y, e.Y) - viewPosition.Y * statusbarZoom.Value,
                    Math.Abs(e.X - boxSelectStartPosition.X),
                    Math.Abs(e.Y - boxSelectStartPosition.Y)
                );

                for (int i = MC.frames[0].placements.Count - 1; i >= 0; i--)
                {
                    var placement = MC.frames[0].placements[i];
                    var shapePos = stage_lm.positions[placement.positionId];

                    RectangleF shape = new RectangleF(
                        (position.X * statusbarZoom.Value + shapePos.X * statusbarZoom.Value - size.X),
                        (position.Y * statusbarZoom.Value + shapePos.Y * statusbarZoom.Value - size.Y),
                        size.X * 2,
                        size.Y * 2
                    );

                    if (selectionRect.IntersectsWith(shape))
                    {
                        selectedIcons.Add(i);
                    }
                }
            }
        }

        private void glControl_MouseWheel(object sender, MouseEventArgs e)
        {
            float oldscale = statusbarZoom.Value;
            float scale = oldscale + ((float)e.Delta) / 2500.0f;
            scale = Math.Min(Math.Max(scale, 0.01f), 1.0f);

            if (scale != oldscale)
            {
                viewPosition.X += (glControl.DisplayRectangle.Width / 2 - mousePosition.X) * 0.05f;
                viewPosition.Y += (glControl.DisplayRectangle.Height / 2 - mousePosition.Y) * 0.05f;
            }

            statusbarZoom.Value = scale;
        }

        private void glControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                selectedIcons.Clear();

            if (e.KeyCode == Keys.ShiftKey)
                shiftHeld = true;

            #region Hotkeys
            if (e.Control)
            {
                if (e.KeyCode == Keys.S)
                    Save();

                if (e.KeyCode == Keys.A)
                {
                    selectedIcons.Clear();

                    for (int i = 0; i < stage_lm.movieclips[67 + currentLayout].frames[0].placements.Count; i++)
                    {
                        selectedIcons.Add(i);
                    }
                }

                if (e.KeyCode == Keys.Z)
                    Undo();

                if (e.KeyCode == Keys.Y)
                    Redo();
            }
            #endregion

            #region Item Nudging
            int delta = (e.Shift ? 10 : 1);

            if (e.KeyCode == Keys.Up)
                moveSelectedBy(0, -delta);

            if (e.KeyCode == Keys.Down)
                moveSelectedBy(0, delta);

            if (e.KeyCode == Keys.Left)
                moveSelectedBy(-delta, 0);

            if (e.KeyCode == Keys.Right)
                moveSelectedBy(delta, 0);
            #endregion
        }
        #endregion

        private void statusbarZoom_SelectedValueChanged(object sender, EventArgs e)
        {
            if ((string)statusbarZoom.ComboBoxControl.SelectedItem == "Fit in Window")
            {
                statusbarZoom.Value = (float)glControl.ClientRectangle.Width / 1920.0f;
            }
        }

        private void comboBoxLayout_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentLayout = comboBoxLayout.SelectedIndex;
            undoStack.Clear();
            redoStack.Clear();
            selectedIcons.Clear();
        }

        public void SwapSelection()
        {
            if (selectedIcons.Count != 2)
                return;

            UndoState state = new UndoState();
            state.text = "Swap Icons";

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;
            }

            stage_lm.positions[state.positions.First().Key] = state.positions.Last().Value;
            stage_lm.positions[state.positions.Last().Key] = state.positions.First().Value;

            undoStack.Add(state);
        }

        #region Alignment Tools
        public void AlignTop()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            float min = float.MaxValue;
            UndoState state = new UndoState();
            state.text = "Align Top";

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;

                if (pos.Y < min)
                {
                    min = pos.Y;
                }
            }

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                stage_lm.positions[placement.positionId] = new Vector2(pos.X, min);
            }

            undoStack.Add(state);
        }

        public void AlignBottom()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            float max = float.MinValue;
            UndoState state = new UndoState();
            state.text = "Align Bottom";

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;

                if (pos.Y > max)
                {
                    max = pos.Y;
                }
            }

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                stage_lm.positions[placement.positionId] = new Vector2(pos.X, max);
            }

            undoStack.Add(state);
        }

        public void AlignLeft()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            float min = float.MaxValue;
            UndoState state = new UndoState();
            state.text = "Align Left";

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;

                if (pos.X < min)
                {
                    min = pos.X;
                }
            }

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                stage_lm.positions[placement.positionId] = new Vector2(min, pos.Y);
            }

            undoStack.Add(state);
        }

        public void AlignRight()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            float max = float.MinValue;
            UndoState state = new UndoState();
            state.text = "Align Right";

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;

                if (pos.X > max)
                {
                    max = pos.X;
                }
            }

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                stage_lm.positions[placement.positionId] = new Vector2(max, pos.Y);
            }

            undoStack.Add(state);
        }

        public void AlignVerticalCenter()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            float avg = 0;
            UndoState state = new UndoState();
            state.text = "Align Vertical Center";

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;
                avg += pos.Y;
            }

            avg /= selectedIcons.Count();

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                stage_lm.positions[placement.positionId] = new Vector2(pos.X, avg);
            }

            undoStack.Add(state);
        }

        public void AlignHorizontalCenter()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            float avg = 0;
            UndoState state = new UndoState();
            state.text = "Align Horizontal Center";

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;
                avg += pos.X;
            }

            avg /= selectedIcons.Count();

            foreach (var sel in selectedIcons)
            {
                var placement = MC.frames[0].placements[sel];
                var pos = stage_lm.positions[placement.positionId];
                stage_lm.positions[placement.positionId] = new Vector2(avg, pos.Y);
            }

            undoStack.Add(state);
        }

        public void DistributeHorizontal()
        {
            if (selectedIcons.Count == 0)
                return;

            SortedDictionary<int, float> idk = new SortedDictionary<int, float>();
            UndoState state = new UndoState();
            state.text = "Distribute Horizontal";

            foreach (var iconId in selectedIcons)
            {
                var placement = MC.frames[0].placements[iconId];
                var pos = stage_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;
                idk[iconId] = pos.X;
            }

            var sortedElements = idk.OrderBy(kv => kv.Value);

            float start = sortedElements.First().Value;
            float end = sortedElements.Last().Value;

            int id = 0;
            foreach (var kv in sortedElements)
            {
                var placement = MC.frames[0].placements[kv.Key];
                float t = ((float)id) / (sortedElements.Count() - 1);

                var pos = stage_lm.positions[placement.positionId];
                stage_lm.positions[placement.positionId] = new Vector2((1 - t) * start + t * end, pos.Y);
                id++;
            }

            undoStack.Add(state);
        }

        public void DistributeVertical()
        {
            if (selectedIcons.Count == 0)
                return;

            var mc = stage_lm.movieclips[67 + currentLayout];
            SortedDictionary<int, float> idk = new SortedDictionary<int, float>();
            UndoState state = new UndoState();
            state.text = "Distribute Vertical";

            foreach (var iconId in selectedIcons)
            {
                var placement = mc.frames[0].placements[iconId];
                var pos = stage_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;
                idk[iconId] = pos.Y;
            }

            var sortedElements = idk.OrderBy(kv => kv.Value);

            float start = sortedElements.First().Value;
            float end = sortedElements.Last().Value;

            int id = 0;
            foreach (var kv in sortedElements)
            {
                var placement = mc.frames[0].placements[kv.Key];
                float t = ((float)id) / (sortedElements.Count() - 1);

                var pos = stage_lm.positions[placement.positionId];
                stage_lm.positions[placement.positionId] = new Vector2(pos.X, (1 - t) * start + t * end);
                id++;
            }

            undoStack.Add(state);
        }
        #endregion

        private void UpdateUndoState()
        {
            if (undoStack.Count == 0)
            {
                MainForm.MenuEditUndo.Text = "Undo";
                MainForm.MenuEditUndo.Enabled = false;
            }
            else
            {
                MainForm.MenuEditUndo.Text = $"Undo {undoStack.Last().text}";
                MainForm.MenuEditUndo.Enabled = true;
            }

            if (redoStack.Count == 0)
            {
                MainForm.MenuEditRedo.Text = "Redo";
                MainForm.MenuEditRedo.Enabled = false;
            }
            else
            {
                MainForm.MenuEditRedo.Text = $"Redo {redoStack.Last().text}";
                MainForm.MenuEditRedo.Enabled = true;
            }
        }

        private void undoStackChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            UpdateUndoState();

            if (args.Action != NotifyCollectionChangedAction.Reset)
            {
                dirty = true;
                TabText = Text + "*";
            }
        }

        public override void OnFocusGained()
        {
            base.OnFocusGained();
            UpdateUndoState();
        }

        public override void Undo()
        {
            if (undoStack.Count == 0)
                return;

            var state = undoStack.Last();
            var redoState = new UndoState();
            redoState.text = state.text;

            foreach (var oldPos in state.positions)
            {
                redoState.positions[oldPos.Key] = stage_lm.positions[oldPos.Key];
                stage_lm.positions[oldPos.Key] = oldPos.Value;
            }

            redoStack.Add(redoState);
            undoStack.RemoveAt(undoStack.Count - 1);
        }

        public override void Redo()
        {
            if (redoStack.Count == 0)
                return;

            var state = redoStack.Last();
            var undoState = new UndoState();
            undoState.text = state.text;

            foreach (var pos in state.positions)
            {
                undoState.positions[pos.Key] = stage_lm.positions[pos.Key];
                stage_lm.positions[pos.Key] = pos.Value;
            }

            undoStack.Add(undoState);
            redoStack.RemoveAt(redoStack.Count - 1);
        }
    }
}
