﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sm4shFileExplorer;
using System.IO;
using System.Diagnostics;

namespace UIBuilder
{
    public class ParamFile
    {
        InputBuffer buf;

        public ParamFile (string filename)
        {
            buf = new InputBuffer(filename);
            buf.ptr = 0x08;
        }

        /// <summary>
        /// Skip next value.
        /// </summary>
        public void skip()
        {
            byte type = buf.readByte();

            switch (type)
            {
                case 0x01:
                case 0x02:
                    buf.skip(1);
                    break;
                case 0x05:
                case 0x06:
                case 0x20:
                    buf.skip(4);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public int readInt()
        {
            buf.skip(1);
            return buf.readInt();
        }

        public uint readUInt()
        {
            buf.skip(1);
            return (uint)buf.readInt();
        }

        public byte readByte()
        {
            buf.skip(1);
            return buf.readByte();
        }
    }

    public class CharacterData
    {
        public List<Entry> entries = new List<Entry>();
        #region names
        public string[] names = new string[] {
            "Miifighter",
            "Miiswordsman",
            "Miigunner",
            "Mario",
            "Donkey",
            "Link",
            "Samus",
            "Yoshi",
            "Kirby",
            "Fox",
            "Pikachu",
            "Luigi",
            "Captain",
            "Ness",
            "Peach",
            "Koopa",
            "Zelda",
            "Sheik",
            "Marth",
            "Gamewatch",
            "Ganon",
            "Falco",
            "Wario",
            "Metaknight",
            "Pit",
            "Szerosuit",
            "Pikmin",
            "Diddy",
            "Dedede",
            "Ike",
            "Lucario",
            "Robot",
            "Toonlink",
            "Lizardon",
            "Sonic",
            "Purin",
            "Drmario",
            "Lucina",
            "Pitb",
            "Rosetta",
            "Wiifit",
            "Littlemac",
            "Murabito",
            "Palutena",
            "Reflet",
            "Duckhunt",
            "KoopaJr",
            "Shulk",
            "Gekkouga",
            "Pacman",
            "Rockman",
            "Mewtwo",
            "Ryu",
            "Lucas",
            "Roy",
            "Cloud",
            "Bayonetta",
            "Kamui"
        };
        #endregion

        public struct Entry
        {
            public int cosmeticId;
            public byte unk1;
            public byte unk2;
            public byte unk3;
            public int seriesId;
            public int unk4;
            public int characterId;
            public int numSlots;
            public int unk5;
            public byte hide;
            public byte isDLC;
            public byte unk6;
            public byte unk7;
            public byte cssPosition;
            public byte unk8;
            public byte unk9;
            public uint unk10;
            public byte unk11;
            public byte unk12;
            public byte unk13;
            public byte unk14;

            public byte[] iconIds;
            public byte[] nameIds;
        }

        public CharacterData()
        {
        }

        public void Load()
        {
            var pf = new ParamFile(Plugin.GetAsset("data/param/ui/ui_character_db.bin"));
            pf.skip();

            List<Entry> tempEntries = new List<Entry>();
            int numEntries = 82;
            for (int i = 0; i < numEntries; i++)
            {
                Entry entry = new Entry();

                entry.cosmeticId = pf.readInt();
                entry.unk1 = pf.readByte();
                entry.unk2 = pf.readByte();
                entry.unk3 = pf.readByte();
                entry.seriesId = pf.readInt();
                entry.unk4 = pf.readInt();
                entry.characterId = pf.readInt();
                entry.numSlots = pf.readInt();
                entry.unk5 = pf.readInt();
                entry.hide = pf.readByte();
                entry.isDLC = pf.readByte();
                entry.unk6 = pf.readByte();
                entry.unk7 = pf.readByte();
                entry.cssPosition = pf.readByte();
                entry.unk8 = pf.readByte();
                entry.unk9 = pf.readByte();
                entry.unk10 = pf.readUInt();
                entry.unk11 = pf.readByte();
                entry.unk12 = pf.readByte();
                entry.unk13 = pf.readByte();
                entry.unk14 = pf.readByte();

                entry.iconIds = new byte[16];
                entry.nameIds = new byte[16];

                for (int j = 0; j < 16; j++)
                {
                    entry.iconIds[j] = pf.readByte();
                }

                for (int j = 0; j < 16; j++)
                {
                    entry.nameIds[j] = pf.readByte();
                }

                tempEntries.Add(entry);
            }

            tempEntries.RemoveAll(entry =>
                (entry.hide == 1 || entry.characterId == -1 || entry.characterId >= names.Length)
            );

            entries = tempEntries.OrderBy(entry => entry.cssPosition).ToList();

            {
                Entry e = new Entry();
                e.cosmeticId = 200;
                e.characterId = -1;
                e.isDLC = 0;
                entries.Add(e);
            }

            {
                Entry e = new Entry();
                e.cosmeticId = 0;
                e.characterId = -1;
                e.isDLC = 0;
                entries.Add(e);
            }
        }
    }
}
