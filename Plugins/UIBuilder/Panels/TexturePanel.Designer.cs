﻿namespace UIBuilder
{
    partial class TexturePanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textureTree = new System.Windows.Forms.TreeView();
            this.textureTreeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.replaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nutBox = new UIBuilder.NutBox();
            this.textureTreeContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // textureTree
            // 
            this.textureTree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textureTree.Location = new System.Drawing.Point(12, 178);
            this.textureTree.Name = "textureTree";
            this.textureTree.Size = new System.Drawing.Size(247, 287);
            this.textureTree.TabIndex = 1;
            this.textureTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.textureTree_AfterSelect);
            this.textureTree.MouseUp += new System.Windows.Forms.MouseEventHandler(this.textureTree_MouseUp);
            // 
            // textureTreeContextMenu
            // 
            this.textureTreeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.replaceToolStripMenuItem});
            this.textureTreeContextMenu.Name = "textureTreeContextMenu";
            this.textureTreeContextMenu.Size = new System.Drawing.Size(125, 26);
            // 
            // replaceToolStripMenuItem
            // 
            this.replaceToolStripMenuItem.Name = "replaceToolStripMenuItem";
            this.replaceToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.replaceToolStripMenuItem.Text = "Replace...";
            this.replaceToolStripMenuItem.Click += new System.EventHandler(this.replaceToolStripMenuItem_Click);
            // 
            // nutBox
            // 
            this.nutBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nutBox.BackColor = System.Drawing.Color.Black;
            this.nutBox.Location = new System.Drawing.Point(12, 12);
            this.nutBox.Name = "nutBox";
            this.nutBox.Size = new System.Drawing.Size(247, 150);
            this.nutBox.TabIndex = 2;
            this.nutBox.VSync = false;
            // 
            // TexturePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 477);
            this.Controls.Add(this.nutBox);
            this.Controls.Add(this.textureTree);
            this.DockAreas = ((WeifenLuo.WinFormsUI.Docking.DockAreas)(((((WeifenLuo.WinFormsUI.Docking.DockAreas.Float | WeifenLuo.WinFormsUI.Docking.DockAreas.DockLeft) 
            | WeifenLuo.WinFormsUI.Docking.DockAreas.DockRight) 
            | WeifenLuo.WinFormsUI.Docking.DockAreas.DockTop) 
            | WeifenLuo.WinFormsUI.Docking.DockAreas.DockBottom)));
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "TexturePanel";
            this.Text = "Textures";
            this.textureTreeContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TreeView textureTree;
        private NutBox nutBox;
        private System.Windows.Forms.ContextMenuStrip textureTreeContextMenu;
        private System.Windows.Forms.ToolStripMenuItem replaceToolStripMenuItem;
    }
}