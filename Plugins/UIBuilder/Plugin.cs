﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Sm4shFileExplorer;
using Sm4shFileExplorer.Globals;
using Sm4shFileExplorer.Objects;
using OpenTK.Graphics;

namespace UIBuilder
{
    public class Plugin : Sm4shBasePlugin
    {
        public static short VersionMajor = 0;
        public static short VersionMinor = 4;
        public static short VersionPatch = 0;
        public static short VersionFlag = (short)'β';
        public static GraphicsContext Context;
        public static Sm4shProject Project;

        #region Properties
        public override string Name
        {
            get { return "UI Builder"; }
        }

        public override string Description
        {
            get { return "Edit lm files and textures."; }
        }

        public override string Research
        {
            get { return "aelan"; }
        }

        public override string GUI
        {
            get { return "aelan"; }
        }

        public override string URL
        {
            get { return "/dev/null"; }
        }

        public override string Version
        {
            get
            {
                return $"{VersionMajor}.{VersionMinor}.{VersionPatch}{(char)VersionFlag}";
            }
        }

        public override bool ShowInPluginList
        {
            get { return true; }
        }

        public override Bitmap[] Icons
        {
            get { return new Bitmap[] { Resources.Resource.icon_uibuilder }; }
        }
#endregion

        public Plugin(Sm4shProject project)
            : base(project)
        {
            Project = project;
        }

        public override bool ResourceSelected(ResourceCollection resCol, string relativePath, string extractedFile)
        {
            if (CanResourceBeLoaded(resCol, relativePath) != -1)
            {
                var form = new MainForm(extractedFile);
                form.ShowDialog(Application.OpenForms[0]);
                return true;
            }

            return false;
        }

        public override void OpenPluginMenu()
        {
#if DEBUG
            //var form = new MainForm(null);
            var form = new MainForm(GetAsset("data/ui/lumen/main/main.lm"));
#else
            var form = new MainForm(null);
#endif
            form.ShowDialog(Application.OpenForms[0]);
        }

        public override int CanResourceBeLoaded(ResourceCollection resCol, string relativePath)
        {
            if (IsFileSupported(relativePath))
                return 0;

            return -1;
        }

        public override bool CanBeLoaded()
        {
            if (Sm4shProject.CurrentProject.Is3DS)
                return false;

            return true;
        }

        public static readonly string[] SupportedExtensions = new string[]
        {
            ".lm",
            ".nut"
        };

        public static bool IsFileSupported(string relativePath)
        {
            foreach (var ext in SupportedExtensions)
            {
                if (relativePath.EndsWith(ext))
                    return true;
            }

            return false;
        }

        public static string GetAsset(string path)
        {
            if (Plugin.Project.GetResource(path) == null)
            {
                return null;
            }

            var exPath = Path.Combine("extract/", path);
            var wsPath = Path.Combine("workspace/content/patch/", path);

            if (File.Exists(wsPath))
            {
                return wsPath;
            }

            if (!File.Exists(exPath))
            {
                Project.ExtractResource(path);
            }

            return exPath;
        }

        public static bool WorkspaceContains(string path)
        {
            return File.Exists(Path.Combine("workspace/content/patch/", path));
        }
    }
}
