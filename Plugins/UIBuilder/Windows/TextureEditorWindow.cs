﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using System.Linq;
using System.Collections.Generic;

namespace UIBuilder
{
    public partial class TextureEditorWindow : BaseWindow
    {
        private Nut nut;
        private string currentFilename;
        private bool dirty = false;

        public ToolStripZoomControl statusbarZoom = new ToolStripZoomControl();

        public TextureEditorWindow(string filename)
        {
            InitializeComponent();

            statusStrip1.Items.Add(new ToolStripLabel("Zoom:"));
            statusStrip1.Items.Add(statusbarZoom);

            currentFilename = filename;
            var split = filename.Split(new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
            Text = split.Skip(split.Length - 2).Take(2).Aggregate((path, part) => path + '/' + part);
            TabText = Text;
        }

        protected override bool UseSave { get { return true; } }
        public override ToolStrip Toolbar {  get { return toolStrip1; } }

        private void Application_Idle(object sender, EventArgs e)
        {
            if (glControl.IsIdle)
            {
                Plugin.Context.MakeCurrent(glControl.WindowInfo);

                GL.PushAttrib(AttribMask.AllAttribBits);
                GL.UseProgram(0);

                GL.Clear(ClearBufferMask.ColorBufferBit);
                GL.Color3(Color.White);

                var ortho = Matrix4.CreateOrthographicOffCenter(
                    0,
                    glControl.ClientRectangle.Width / statusbarZoom.Value,
                    glControl.ClientRectangle.Height /statusbarZoom.Value,
                    0,
                    0, 10
                );

                GL.MatrixMode(MatrixMode.Projection);
                GL.PushMatrix();
                GL.LoadIdentity();
                GL.LoadMatrix(ref ortho);
                GL.Viewport(glControl.ClientRectangle);

                GL.MatrixMode(MatrixMode.Modelview);
                GL.PushMatrix();
                GL.LoadIdentity();

                GL.Translate(glControl.ClientRectangle.Width / 2, glControl.ClientRectangle.Height / 2, 0);

                float w = nut.width / 2;
                float h = nut.height / 2;

                GL.BindTexture(TextureTarget.Texture2D, nut.glId);
                GL.Begin(PrimitiveType.Quads);
                GL.TexCoord2(0, 0);
                GL.Vertex2(-w, -h);
                GL.TexCoord2(1, 0);
                GL.Vertex2(w, -h);
                GL.TexCoord2(1, 1);
                GL.Vertex2(w, h);
                GL.TexCoord2(0, 1);
                GL.Vertex2(-w, h);
                GL.End();

                GL.PopMatrix();
                GL.MatrixMode(MatrixMode.Projection);
                GL.PopMatrix();
                GL.MatrixMode(MatrixMode.Modelview);
                GL.PopAttrib();

                glControl.SwapBuffers();
            }
        }

        public override void Save()
        {
            if (dirty)
            {
                dirty = false;
                TabText = Text;
            }

            using (var stream = new FileStream(currentFilename, FileMode.Create))
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(nut.Rebuild());
            }
        }

        private void TextureEditorWindow_Load(object sender, EventArgs e)
        {
            Plugin.Context.MakeCurrent(glControl.WindowInfo);

            nut = new Nut(currentFilename);

            GL.ClearColor(Color.Black);
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            Application.Idle += Application_Idle;
        }

        private void TextureEditorWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Idle -= Application_Idle;
        }

        private void replaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "All Graphics Types|*.png;*.jpg;*.jpeg;*.bmp;*.tif;*.tiff|" +
                "PNG|*.png|JPG|*.jpg;*.jpeg|GIF|*.gif|BMP|*.bmp|TIFF|*.tif;*.tiff";
            var result = ofd.ShowDialog();

            if (result != DialogResult.OK)
                return;

            nut.Replace(new Bitmap(ofd.FileName));

            dirty = true;
            TabText = Text + "*";
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("test");
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.Filter = "All Graphics Types|*.nut;*.png;*.jpg;*.jpeg;*.bmp;*.tif;*.tiff|" +
                "NUT|*.nut|PNG|*.png|JPG|*.jpg;*.jpeg|GIF|*.gif|BMP|*.bmp|TIFF|*.tif;*.tiff";
            var result = sfd.ShowDialog();

            if (result != DialogResult.OK)
                return;

            nut.Export(sfd.FileName);
        }
    }
}
