﻿using System;
using System.Collections.Generic;
using OpenTK;

namespace UIBuilder
{
    public class Texlist
    {
        public class Texture
        {
            public enum Flags : int
            {
                None = 0x00000000,
                Dynamic = 0x01000000
            }

            public string name;

            public Flags flags;

            public Vector2 topLeft;
            public Vector2 botRight;

            public short width;
            public short height;

            public short atlasId;
        }

        public List<Texture> textures { get; set; }
        public int numAtlases;

        public Texlist()
        {
            textures = new List<Texture>();
            numAtlases = 0;
        }

        public Texlist(string filename) : this()
        {
            Read(filename);
        }

        public void Read(string filename)
        {
            InputBuffer buf = new InputBuffer(filename);

            buf.ptr = 0x06;

            numAtlases = buf.readShort();
            short numTextures = buf.readShort();
            short flagsOffset = buf.readShort();
            short entriesOffset = buf.readShort();
            short stringsOffset = buf.readShort();

            List<Texture.Flags> flags = new List<Texture.Flags>();

            buf.ptr = (uint)flagsOffset;
            for (int i = 0; i < numTextures; i++)
            {
                flags.Add((Texture.Flags)buf.readInt());
            }

            buf.ptr = (uint)entriesOffset;
            for (int i = 0; i < numTextures; i++)
            {
                Texture entry = new Texture();
                int nameOffset = buf.readInt();
                int nameOffset2 = buf.readInt();

                // I have yet to see this.
                if (nameOffset != nameOffset2)
                {
                    throw new NotImplementedException("texlist name offsets don't match?");
                }

                entry.name = buf.readString(stringsOffset + nameOffset);
                entry.flags = flags[i];

                entry.topLeft = new Vector2(buf.readFloat(), buf.readFloat());
                entry.botRight = new Vector2(buf.readFloat(), buf.readFloat());

                entry.width = buf.readShort();
                entry.height = buf.readShort();
                entry.atlasId = buf.readShort();

                textures.Add(entry);

                buf.skip(0x02); // Padding.
            }
        }
    }
}
