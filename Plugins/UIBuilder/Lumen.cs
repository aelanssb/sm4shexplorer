﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Text;
using System.Drawing;

namespace UIBuilder
{
    public class Lumen
    {
        public enum ChunkType : int
        {
            Invalid = 0x0000,

            Unk000A = 0x000A,
            DynamicText = 0x0025,
            Symbols = 0xF001,
            Colors = 0xF002,
            Transforms = 0xF003,
            Bounds = 0xF004,
            ActionScript = 0xF005,
            TextureAtlases = 0xF007,
            UnkF008 = 0xF008,
            UnkF009 = 0xF009,
            UnkF00A = 0xF00A,
            UnkF00B = 0xF00B,
            Properties = 0xF00C,
            UnkF00D = 0xF00D,
            Shape = 0xF022,
            Graphic = 0xF024,
            Positions = 0xF103,

            MovieClip = 0x0027,
            Label = 0x002B,
            Frame = 0x0001,
            Keyframe = 0xF105,
            ObjectPlacement = 0x0004,
            ObjectDeletion = 0x0005,
            Action = 0x000C,

            End = 0xFF00
        }

        public abstract class LumenObject { }

        public class UnhandledChunk
        {
            public UnhandledChunk()
            {
                type = ChunkType.Invalid;
            }

            public UnhandledChunk(ChunkType type, int size, InputBuffer f)
            {
                this.type = type;
                this.size = size;
                data = f.read(size * 4);
            }

            public UnhandledChunk(InputBuffer f)
            {
                type = (ChunkType)f.readInt();
                size = f.readInt();
                data = f.read(size * 4);
            }

            public void Write(OutputBuffer o)
            {
                o.writeInt((int)type);
                o.writeInt(size);
                o.write(data);
            }

            public ChunkType type;
            public int size;

            byte[] data;
        }

        public struct TextureAtlas
        {
            public int id;
            public int unk;

            public float width;
            public float height;
        }

        public class Graphic
        {
            public int nameId;
            public int atlasId;
            public short unk1;

            public Vector4[] verts;
            public short[] indices;

            public int vbo = 0;
            public int ibo = 0;

            public Graphic()
            {

            }

            public Graphic(InputBuffer f)
            {
                Read(f);
            }

            public void Read(InputBuffer f)
            {
                atlasId = f.readInt();
                unk1 = f.readShort();

                int numVerts = f.readShort();
                int numIndices = f.readInt();

                verts = new Vector4[numVerts];
                indices = new short[numIndices];

                for (int i = 0; i < numVerts; i++)
                    verts[i] = new Vector4(f.readFloat(), f.readFloat(), f.readFloat(), f.readFloat());

                for (int i = 0; i < numIndices; i++)
                    indices[i] = f.readShort();

                // indices are padded to word boundaries
                if ((numIndices % 2) != 0)
                    f.skip(0x02);

                GL.GenBuffers(1, out vbo);
                GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(verts.Length * Vector4.SizeInBytes), verts, BufferUsageHint.StaticDraw);
                GL.GenBuffers(1, out ibo);
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, ibo);
                GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indices.Length * sizeof(short)), indices, BufferUsageHint.StaticDraw);
            }

            public void Write(OutputBuffer o)
            {
                OutputBuffer chunk = new OutputBuffer();
                chunk.writeInt(atlasId);
                chunk.writeShort(unk1);
                chunk.writeShort((short)verts.Length);
                chunk.writeInt(indices.Length);

                foreach (var vert in verts)
                {
                    chunk.writeFloat(vert.X);
                    chunk.writeFloat(vert.Y);
                    chunk.writeFloat(vert.Z);
                    chunk.writeFloat(vert.W);
                }

                foreach (var index in indices)
                {
                    chunk.writeShort(index);
                }

                if ((indices.Length % 2) != 0)
                {
                    chunk.writeShort(0);
                }

                o.writeInt((int)ChunkType.Graphic);
                o.writeInt(chunk.Size / 4);
                o.write(chunk);
            }
        }

        public class Shape : LumenObject
        {
            public int id;
            public int unk1;
            public int boundsId;
            public int unk2;

            public Vector4 bounds;
            public Graphic[] graphics;

            public Shape()
            {

            }

            public Shape(InputBuffer f)
            {
                Read(f);
            }

            public void Read(InputBuffer f)
            {
                id = f.readInt();
                unk1 = f.readInt();
                boundsId = f.readInt();
                unk2 = f.readInt();

                int numGraphics = f.readInt();
                graphics = new Graphic[numGraphics];

                for (int i = 0; i < numGraphics; i++)
                {
                    f.skip(0x08); // graphic chunk header
                    graphics[i] = new Graphic(f);
                }
            }

            public void Write(OutputBuffer o)
            {
                o.writeInt((int)ChunkType.Shape);
                o.writeInt(5);

                o.writeInt(id);
                o.writeInt(unk1);
                o.writeInt(boundsId);
                o.writeInt(unk2);
                o.writeInt(graphics.Length);

                foreach (var graphic in graphics)
                {
                    graphic.Write(o);
                }
            }
        }

        public class DynamicText : LumenObject
        {
            public enum Alignment : short
            {
                Left = 0,
                Right = 1,
                Center = 2
            }

            public int id;
            public int unk1;
            public int placeholderTextId;
            public string placeholder; // FIXME
            public int unk2;
            public int colorId;
            public int unk3;
            public int unk4;
            public int unk5;
            public Alignment alignment;
            public short unk6;
            public int unk7;
            public int unk8;
            public float size;
            public int unk9;
            public int unk10;
            public int unk11;
            public int unk12;

            public DynamicText() { }

            public DynamicText (InputBuffer f)
            {
                Read(f);
            }

            public void Read(InputBuffer f)
            {
                id = f.readInt();
                unk1 = f.readInt();
                placeholderTextId = f.readInt();
                unk2 = f.readInt();
                colorId = f.readInt();
                unk3 = f.readInt();
                unk4 = f.readInt();
                unk5 = f.readInt();
                alignment = (Alignment)f.readShort();
                unk6 = f.readShort();
                unk7 = f.readInt();
                unk8 = f.readInt();
                size = f.readFloat();
                unk9 = f.readInt();
                unk10 = f.readInt();
                unk11 = f.readInt();
                unk12 = f.readInt();
            }

            public void Write(OutputBuffer o)
            {
                o.writeInt((int)ChunkType.DynamicText);
                o.writeInt(16);

                o.writeInt(id);
                o.writeInt(unk1);
                o.writeInt(placeholderTextId);
                o.writeInt(unk2);
                o.writeInt(colorId);
                o.writeInt(unk3);
                o.writeInt(unk4);
                o.writeInt(unk5);
                o.writeShort((short)alignment);
                o.writeShort(unk6);
                o.writeInt(unk7);
                o.writeInt(unk8);
                o.writeFloat(size);
                o.writeInt(unk9);
                o.writeInt(unk10);
                o.writeInt(unk11);
                o.writeInt(unk12);
            }
        }

        public class MovieClip : LumenObject
        {
            public class Label
            {
                public int nameId;
                public int startFrame;
                public int unk1;

                public Label(InputBuffer f)
                {
                    Read(f);
                }

                public void Read(InputBuffer f)
                {
                    nameId = f.readInt();
                    startFrame = f.readInt();
                    unk1 = f.readInt();
                }

                public void Write(OutputBuffer o)
                {
                    o.writeInt((int)ChunkType.Label);
                    o.writeInt(3);
                    o.writeInt(nameId);
                    o.writeInt(startFrame);
                    o.writeInt(unk1);
                }
            }

            public class Placement
            {
                public int objectId;
                public int placementId;
                public int unk1;
                public int nameId;
                public short _maybeBlendMode;
                public short unk3;
                public short mcObjectId;
                public short unk4;

                public short unk5;
                public short unk6;
                public short positionFlags;
                public short positionId;
                public int colorId1;
                public int colorId2;

                public List<UnhandledChunk> unkF037s { get; set; }
                public List<UnhandledChunk> unkF014s { get; set; }

                public Placement()
                {
                    unkF037s = new List<UnhandledChunk>();
                    unkF014s = new List<UnhandledChunk>();
                }

                public Placement(InputBuffer f) : this()
                {
                    Read(f);
                }

                public void Read(InputBuffer f)
                {
                    objectId = f.readInt();
                    placementId = f.readInt();
                    unk1 = f.readInt();
                    nameId = f.readInt();
                    _maybeBlendMode = f.readShort();
                    unk3 = f.readShort();
                    mcObjectId = f.readShort();
                    unk4 = f.readShort();
                    unk5 = f.readShort();
                    unk6 = f.readShort();
                    positionFlags = f.readShort();
                    positionId = f.readShort();
                    colorId1 = f.readInt();
                    colorId2 = f.readInt();

                    int numF037s = f.readInt();
                    int numF014s = f.readInt();

                    for (int i = 0; i < numF037s; i++)
                    {
                        unkF037s.Add(new UnhandledChunk(f));
                    }

                    for (int i = 0; i < numF014s; i++)
                    {
                        unkF014s.Add(new UnhandledChunk(f));
                    }
                }

                public void Write(OutputBuffer o)
                {
                    o.writeInt((int)ChunkType.ObjectPlacement);
                    o.writeInt(12);

                    o.writeInt(objectId);
                    o.writeInt(placementId);
                    o.writeInt(unk1);
                    o.writeInt(nameId);
                    o.writeShort(_maybeBlendMode);
                    o.writeShort(unk3);
                    o.writeShort(mcObjectId);
                    o.writeShort(unk4);
                    o.writeShort(unk5);
                    o.writeShort(unk6);
                    o.writeShort(positionFlags);
                    o.writeShort(positionId);
                    o.writeInt(colorId1);
                    o.writeInt(colorId2);

                    o.writeInt(unkF037s.Count);
                    o.writeInt(unkF014s.Count);

                    foreach (var chunk in unkF037s)
                    {
                        chunk.Write(o);
                    }

                    foreach (var chunk in unkF014s)
                    {
                        chunk.Write(o);
                    }
                }
            }

            public class Deletion
            {
                public int unk1;
                public short mcObjectId; // or was it placement id?
                public short unk2;

                public Deletion()
                {
                }

                public Deletion(InputBuffer f)
                {
                    Read(f);
                }

                public void Read(InputBuffer f)
                {
                    unk1 = f.readInt();
                    mcObjectId = f.readShort();
                    unk2 = f.readShort();
                }

                public void Write(OutputBuffer o)
                {
                    o.writeInt((int)ChunkType.ObjectDeletion);
                    o.writeInt(2);
                    o.writeInt(unk1);
                    o.writeShort(mcObjectId);
                    o.writeShort(unk2);
                }
            }

            public class Action
            {
                public int actionId;
                public int unk1;

                public Action(InputBuffer f)
                {
                    Read(f);
                }

                public void Read(InputBuffer f)
                {
                    actionId = f.readInt();
                    unk1 = f.readInt();
                }

                public void Write(OutputBuffer o)
                {
                    o.writeInt((int)ChunkType.Action);
                    o.writeInt(2);
                    o.writeInt(actionId);
                    o.writeInt(unk1);
                }
            }

            public class Frame
            {
                public int id;

                public List<Deletion> deletions { get; set; }
                public List<Action> actions { get; set; }
                public List<Placement> placements { get; set; }

                public Frame()
                {
                    deletions = new List<Deletion>();
                    actions = new List<Action>();
                    placements = new List<Placement>();
                }

                public Frame(InputBuffer f) : this()
                {
                    Read(f);
                }

                public void Read(InputBuffer f)
                {
                    id = f.readInt();
                    int numChildren = f.readInt();

                    for (int childId = 0; childId < numChildren; childId++)
                    {
                        ChunkType childType = (ChunkType)f.readInt();
                        int childSize = f.readInt();

                        if (childType == ChunkType.ObjectDeletion)
                            deletions.Add(new Deletion(f));
                        else if (childType == ChunkType.Action)
                            actions.Add(new Action(f));
                        else if (childType == ChunkType.ObjectPlacement)
                            placements.Add(new Placement(f));
                    }
                }

                // NOTE: unlike other chunk write functions, this does not include the header
                // so it can be used for both frames and keyframes.
                public void Write(OutputBuffer o)
                {
                    o.writeInt(id);
                    o.writeInt(deletions.Count + actions.Count + placements.Count);

                    foreach (var deletion in deletions)
                    {
                        deletion.Write(o);
                    }

                    foreach (var action in actions)
                    {
                        action.Write(o);
                    }

                    foreach (var placement in placements)
                    {
                        placement.Write(o);
                    }
                }
            }

            public int id;
            public int unk1;
            public int unk2;
            public int unk3;

            public Label[] labels;
            public List<Frame> frames { get; set; }
            public List<Frame> keyframes { get; set; }

            public MovieClip()
            {
                frames = new List<Frame>();
                keyframes = new List<Frame>();
            }

            public MovieClip(InputBuffer f) : this()
            {
                Read(f);
            }

            public void Read(InputBuffer f)
            {
                id = f.readInt();
                unk1 = f.readInt();
                unk2 = f.readInt();

                int numLabels = f.readInt();
                int numFrames = f.readInt();
                int numKeyframes = f.readInt();

                labels = new Label[numLabels];

                unk3 = f.readInt();

                for (int i = 0; i < numLabels; i++)
                {
                    f.skip(0x08);

                    labels[i] = new Label(f);
                }

                int totalFrames = numFrames + numKeyframes;
                for (int frameId = 0; frameId < totalFrames; frameId++)
                {
                    ChunkType frameType = (ChunkType)f.readInt();
                    f.skip(0x04);

                    Frame frame = new Frame(f);

                    if (frameType == ChunkType.Keyframe)
                    {
                        keyframes.Add(frame);
                    }
                    else
                    {
                        frames.Add(frame);
                    }
                }
            }

            public void Write(OutputBuffer o)
            {
                o.writeInt((int)ChunkType.MovieClip);
                o.writeInt(7);
                o.writeInt(id);
                o.writeInt(unk1);
                o.writeInt(unk2);
                o.writeInt(labels.Length);
                o.writeInt(frames.Count);
                o.writeInt(keyframes.Count);
                o.writeInt(unk3);

                foreach (var label in labels)
                {
                    label.Write(o);
                }

                foreach (var frame in frames)
                {
                    o.writeInt((int)ChunkType.Frame);
                    o.writeInt(2);
                    frame.Write(o);
                }

                foreach (var frame in keyframes)
                {
                    o.writeInt((int)ChunkType.Keyframe);
                    o.writeInt(2);
                    frame.Write(o);
                }
            }
        }

        public class Header
        {
            public int magic;
            public int unk0;
            public int unk1;
            public int unk2;
            public int unk3;
            public int unk4;
            public int unk5;
            public int filesize;
            public int unk6;
            public int unk7;
            public int unk8;
            public int unk9;
            public int unk10;
            public int unk11;
            public int unk12;
            public int unk13;

            public void Write(OutputBuffer o)
            {
                o.writeInt(magic);
                o.writeInt(unk0);
                o.writeInt(unk1);
                o.writeInt(unk2);
                o.writeInt(unk3);
                o.writeInt(unk4);
                o.writeInt(unk5);
                o.writeInt(filesize);
                o.writeInt(unk6);
                o.writeInt(unk7);
                o.writeInt(unk8);
                o.writeInt(unk9);
                o.writeInt(unk10);
                o.writeInt(unk11);
                o.writeInt(unk12);
                o.writeInt(unk13);
            }
        }

        public Header header = new Header();
        public List<string> symbols = new List<string>();
        public List<Vector4> colors = new List<Vector4>();
        public List<Matrix4> transforms = new List<Matrix4>();
        public List<Vector2> positions = new List<Vector2>();
        public List<Vector4> bounds = new List<Vector4>();
        public List<TextureAtlas> textureAtlases = new List<TextureAtlas>();
        public List<Shape> shapes = new List<Shape>();
        public List<DynamicText> texts = new List<DynamicText>();
        public List<MovieClip> movieclips = new List<MovieClip>();

        public Dictionary<int, LumenObject> objects = new Dictionary<int, LumenObject>();

        public UnhandledChunk properties;
        public UnhandledChunk actionscript;
        public UnhandledChunk unkF008;
        public UnhandledChunk unkF009;
        public UnhandledChunk unkF00A;
        public UnhandledChunk unk000A;
        public UnhandledChunk unkF00B;
        public UnhandledChunk unkF00D;

        public Lumen()
        {

        }

        public Lumen (string filename)
        {
            Read(filename);
        }

        public void Read (string filename)
        {
            InputBuffer f = new InputBuffer(filename);
            header.magic = f.readInt();
            header.unk0 = f.readInt();
            header.unk1 = f.readInt();
            header.unk2 = f.readInt();
            header.unk3 = f.readInt();
            header.unk4 = f.readInt();
            header.unk5 = f.readInt();
            header.filesize = f.readInt();
            header.unk6 = f.readInt();
            header.unk7 = f.readInt();
            header.unk8 = f.readInt();
            header.unk9 = f.readInt();
            header.unk10 = f.readInt();
            header.unk11 = f.readInt();
            header.unk12 = f.readInt();
            header.unk13 = f.readInt();

            bool done = false;
            while (!done)
            {
                uint chunkOffset = f.ptr;
                ChunkType chunkType = (ChunkType)f.readInt();
                int chunkSize = f.readInt(); // in dwords!

                switch (chunkType)
                {
                    case ChunkType.Invalid:
                        // uhhh. i think there's a specific exception for this
                        throw new Exception("Malformed file");

                    case ChunkType.Symbols:
                        int numSymbols = f.readInt();

                        while (symbols.Count < numSymbols)
                        {
                            int len = f.readInt();

                            symbols.Add(f.readString());
                            f.skip(4 - (f.ptr % 4));
                        }

                        break;

                    case ChunkType.Colors:
                        int numColors = f.readInt();

                        for (int i = 0; i < numColors; i++)
                        {
                            short r = f.readShort();
                            short g = f.readShort();
                            short b = f.readShort();
                            short a = f.readShort();

                            var color = new Vector4();
                            color.X = (r == 0) ? -1f : (r - 1) / 255f;
                            color.Y = (g == 0) ? -1f : (g - 1) / 255f;
                            color.Z = (b == 0) ? -1f : (b - 1) / 255f;
                            color.W = (a == 0) ? -1f : (a - 1) / 255f;
                            colors.Add(color);
                        }
                        break;

                    case ChunkType.Unk000A:
                        unk000A = new UnhandledChunk(chunkType, chunkSize, f);
                        break;
                    case ChunkType.UnkF00A:
                        unkF00A = new UnhandledChunk(chunkType, chunkSize, f);
                        break;
                    case ChunkType.UnkF00B:
                        unkF00B = new UnhandledChunk(chunkType, chunkSize, f);
                        break;
                    case ChunkType.UnkF008:
                        unkF008 = new UnhandledChunk(chunkType, chunkSize, f);
                        break;
                    case ChunkType.UnkF009:
                        unkF009 = new UnhandledChunk(chunkType, chunkSize, f);
                        break;
                    case ChunkType.UnkF00D:
                        unkF00D = new UnhandledChunk(chunkType, chunkSize, f);
                        break;
                    case ChunkType.ActionScript:
                        actionscript = new UnhandledChunk(chunkType, chunkSize, f);
                        break;

                    case ChunkType.End:
                        done = true;
                        break;

                    case ChunkType.Transforms:
                        int numTransforms = f.readInt();

                        for (int i = 0; i < numTransforms; i++)
                        {
                            transforms.Add(new Matrix4(
                                f.readFloat(), f.readFloat(), 0f, 0f,
                                f.readFloat(), f.readFloat(), 0f, 0f,
                                f.readFloat(), f.readFloat(), 1f, 0f,
                                0f, 0, 0, 1f
                            ));
                        }

                        break;

                    case ChunkType.Positions:
                        int numPositions = f.readInt();

                        for (int i = 0; i < numPositions; i++)
                            positions.Add(new Vector2(f.readFloat(), f.readFloat()));

                        break;

                    case ChunkType.Bounds:
                        int numBounds = f.readInt();

                        for (int i = 0; i < numBounds; i++)
                            bounds.Add(new Vector4(f.readFloat(), f.readFloat(), f.readFloat(), f.readFloat()));

                        break;

                    case ChunkType.Properties:
                        properties = new UnhandledChunk(chunkType, chunkSize, f);
                        break;

                    case ChunkType.TextureAtlases:
                        int numAtlases = f.readInt();

                        for (int i = 0; i < numAtlases; i++)
                        {
                            TextureAtlas atlas = new TextureAtlas();
                            atlas.id = f.readInt();
                            atlas.unk = f.readInt();
                            atlas.width = f.readFloat();
                            atlas.height = f.readFloat();

                            textureAtlases.Add(atlas);
                        }

                        break;

                    case ChunkType.Shape:
                        var shape = new Shape(f);
                        shape.bounds = bounds[shape.boundsId];
                        shapes.Add(shape);
                        objects[shape.id] = shape;
                        break;

                    case ChunkType.DynamicText:
                        var text = new DynamicText(f);
                        text.placeholder = symbols[text.placeholderTextId];
                        texts.Add(text);
                        objects[text.id] = text;
                        break;

                    case ChunkType.MovieClip:
                        var mc = new MovieClip(f);
                        movieclips.Add(mc);
                        objects[mc.id] = mc;
                        break;

                    default:
                        throw new NotImplementedException($"Unhandled chunk id: 0x{(uint)chunkType:X} @ 0x{chunkOffset:X}");
                }
            }
        }

        #region serialization
        void writeSymbols(OutputBuffer o)
        {
            OutputBuffer chunk = new OutputBuffer();
            chunk.writeInt(symbols.Count);

            foreach (var symbol in symbols)
            {
                chunk.writeInt(symbol.Length);
                chunk.writeString(symbol);

                int padSize = 4 - (chunk.Size % 4);
                for (int i = 0; i < padSize; i++)
                {
                    chunk.writeByte(0);
                }
            }

            o.writeInt((int)ChunkType.Symbols);
            o.writeInt(chunk.Size / 4);
            o.write(chunk);
        }

        void writeColors(OutputBuffer o)
        {
            o.writeInt((int)ChunkType.Colors);
            o.writeInt(colors.Count * 2 + 1);
            o.writeInt(colors.Count);

            foreach (var color in colors)
            {
                o.writeShort((short)((color.X < 0) ? 0 : color.X * 0xFF + 1));
                o.writeShort((short)((color.Y < 0) ? 0 : color.Y * 0xFF + 1));
                o.writeShort((short)((color.Z < 0) ? 0 : color.Z * 0xFF + 1));
                o.writeShort((short)((color.W < 0) ? 0 : color.W * 0xFF + 1));
            }
        }

        void writePositions(OutputBuffer o)
        {
            o.writeInt((int)ChunkType.Positions);
            o.writeInt(positions.Count * 2 + 1);
            o.writeInt(positions.Count);

            foreach (var position in positions)
            {
                o.writeFloat(position.X);
                o.writeFloat(position.Y);
            }
        }

        void writeTransforms(OutputBuffer o)
        {
            o.writeInt((int)ChunkType.Transforms);
            o.writeInt(transforms.Count * 6 + 1);
            o.writeInt(transforms.Count);

            foreach (var transform in transforms)
            {
                o.writeFloat(transform.M11);
                o.writeFloat(transform.M12);
                o.writeFloat(transform.M21);
                o.writeFloat(transform.M22);
                o.writeFloat(transform.M31);
                o.writeFloat(transform.M32);
            }
        }

        void writeBounds(OutputBuffer o)
        {
            o.writeInt((int)ChunkType.Bounds);
            o.writeInt(bounds.Count * 4 + 1);
            o.writeInt(bounds.Count);

            foreach (var bb in bounds)
            {
                o.writeFloat(bb.X);
                o.writeFloat(bb.Y);
                o.writeFloat(bb.Z);
                o.writeFloat(bb.W);
            }
        }

        void writeAtlases(OutputBuffer o)
        {
            o.writeInt((int)ChunkType.TextureAtlases);
            o.writeInt(textureAtlases.Count * 4 + 1);
            o.writeInt(textureAtlases.Count);

            foreach (var atlas in textureAtlases)
            {
                o.writeInt(atlas.id);
                o.writeInt(atlas.unk);
                o.writeFloat(atlas.width);
                o.writeFloat(atlas.height);
            }
        }

        void writeShapes(OutputBuffer o)
        {
            shapes.ForEach(shape => shape.Write(o));
        }

        void writeMovieClips(OutputBuffer o)
        {
            movieclips.ForEach(mc => mc.Write(o));
        }

        void writeTexts(OutputBuffer o)
        {
            texts.ForEach(text => text.Write(o));
        }

        #endregion

        public void SaveMetadata()
        {
            //bool fileModded = false;
            //int metadataColorId = -1;

            //const short magic1 = 0x4255;
            //const short magic2 = 0x5454;
            //const short configSize = 1; // in color entries
            //const short reserved = 0;

            //for (int i = 0; i < colors.Count; i++)
            //{
            //    if (colors[i].r == magic1 && colors[i].g == magic2)
            //    {
            //        metadataColorId = i;
            //        fileModded = true;
            //        break;
            //    }
            //}

            //if (fileModded)
            //{
            //    colors[metadataColorId + 1].r = Plugin.VersionMajor;
            //    colors[metadataColorId + 1].g = Plugin.VersionMinor;
            //    colors[metadataColorId + 1].b = Plugin.VersionPatch;
            //    colors[metadataColorId + 1].a = Plugin.VersionFlag;
            //}
            //else
            //{
            //    metadataColorId = colors.Count;
            //    colors.Add(new Lumen.Color(magic1, magic2, configSize, reserved));
            //    colors.Add(new Lumen.Color(
            //        Plugin.VersionMajor,
            //        Plugin.VersionMinor,
            //        Plugin.VersionPatch,
            //        Plugin.VersionFlag
            //    ));
            //}
        }

        public byte[] Rebuild()
        {
            OutputBuffer o = new OutputBuffer();

            // TODO: write correct filesize in header. 
            // It isn't checked by the game, but what the hell, right?
            header.Write(o);

            writeSymbols(o);
            writeColors(o);
            writeTransforms(o);
            writePositions(o);
            writeBounds(o);
            actionscript.Write(o);
            writeAtlases(o);

            unkF008.Write(o);
            unkF009.Write(o);
            unkF00A.Write(o);
            unk000A.Write(o);
            unkF00B.Write(o);
            properties.Write(o);
            unkF00D.Write(o);

            writeShapes(o);
            writeMovieClips(o);
            writeTexts(o);

            o.writeInt((int)ChunkType.End);
            o.writeInt(0);

            int padSize = (4 - (o.Size % 4)) % 4;
            for (int i = 0; i < padSize; i++)
                o.writeByte(0);

            return o.getBytes();
        }
    }
}
