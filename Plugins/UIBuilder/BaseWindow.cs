﻿using System.ComponentModel;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace UIBuilder
{
    public class BaseWindow : DockContent
    {
        protected virtual bool UseUndo { get { return false; } }
        protected virtual bool UseSave { get { return false; } }
        public virtual ToolStrip Toolbar { get { return null; } }

        public virtual void OnFocusGained()
        {
            MainForm.MenuFileSave.Enabled = UseSave;

            MainForm.MenuEditUndo.Enabled = UseUndo;
            MainForm.MenuEditRedo.Enabled = UseUndo;

            MainForm.MenuEditUndo.Text = "Undo";
            MainForm.MenuEditRedo.Text = "Redo";
        }

        public virtual void OnFocusLost() { }
        public virtual void Save() { }
        public virtual void Undo() { }
        public virtual void Redo() { }
    }
}
