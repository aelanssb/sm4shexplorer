﻿using System;
using System.Collections.Specialized;
using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using OpenTK.Graphics.OpenGL;
using System.Linq;

namespace UIBuilder
{
    public partial class TexturePanel : DockContent
    {
        private Texlist texlist;

        public TexturePanel()
        {
            InitializeComponent();

            //Plugin.TextureAtlases.CollectionChanged += OnAtlasesChanged;

            texlist = new Texlist(Plugin.GetAsset("data/ui/lumen/chara/texlist.lst"));

            var ids = texlist.textures.Select(t => t.atlasId).Distinct();
            foreach (var atlasId in ids)
            {
                var node = new TreeNode();
                node.Text = $"{atlasId:d5}";
                node.Tag = (int)atlasId;
                node.ForeColor = Color.Gray;
                textureTree.Nodes.Add(node);
            }

            foreach (var texture in texlist.textures)
            {
                var node = new TreeNode();
                node.Text = texture.name;
                node.Tag = texture;

                textureTree.Nodes[texture.atlasId].Nodes.Add(node);
            }

            //for (int i = 0; i < Plugin.TextureAtlases.Count; i++)
            //{
            //    if (Plugin.TextureAtlases[i] == null)
            //    {
            //        textureTree.Nodes[i].Text = $"Dynamic Texture {i}";
            //    }
            //    else
            //    {
            //        textureTree.Nodes[i].ForeColor = Color.Black;
            //        textureTree.Nodes[i].Text = $"img-{i:d5}.nut";
            //    }
            //}
        }

        ~TexturePanel()
        {
            //Plugin.TextureAtlases.CollectionChanged -= OnAtlasesChanged;
        }

        private void OnAtlasesChanged (object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                textureTree.Nodes.Clear();
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {

            }
            else
            {
                int i = e.NewStartingIndex;
                foreach (var atlas in e.NewItems)
                {
                    if (atlas != null)
                    {
                        textureTree.Nodes[i].ForeColor = Color.Black;
                        textureTree.Nodes[i].Text = $"img-{i:d5}.nut";
                    }
                    else
                    {
                        textureTree.Nodes[i].Text = $"Dynamic Texture {i}";
                    }

                    i++;
                }
            }
        }

        private void textureTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //Nut nut = null;
            //if (e.Node.Level == 0)
            //{
            //    var atlasId = (int)e.Node.Tag;
            //    if (atlasId < Plugin.TextureAtlases.Count)
            //        nut = Plugin.TextureAtlases[atlasId];

            //    nutBox.setNut(nut, OpenTK.Vector2.Zero, OpenTK.Vector2.One);
            //}
            //else
            //{
            //    var texture = (Texlist.Texture)e.Node.Tag;
            //    if (texture.atlasId < Plugin.TextureAtlases.Count)
            //        nut = Plugin.TextureAtlases[texture.atlasId];

            //    nutBox.setNut(nut, texture.topLeft, texture.botRight);
            //}
        }

        private void replaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (textureTree.SelectedNode == null || textureTree.SelectedNode.Level > 0)
                return;

            var ofd = new OpenFileDialog();
            ofd.Filter = "All Graphics Types|*.png;*.jpg;*.jpeg;*.bmp;*.tif;*.tiff" +
                "PNG|*.png|JPG|*.jpg;*.jpeg|GIF|*.gif|BMP|*.bmp|TIFF|*.tif;*.tiff|";
            var result = ofd.ShowDialog();

            if (result != DialogResult.OK)
                return;

            int atlasId = textureTree.SelectedNode.Index;
            //var nut = Plugin.TextureAtlases[atlasId];

            //if (nut == null)
            //{
            //    nut = new Nut();
            //    nut.id = atlasId;
            //    nut.type = PixelInternalFormat.CompressedRgbaS3tcDxt5Ext;
            //}

            //nut.Replace(new Bitmap(ofd.FileName));
            //Plugin.TextureAtlases[atlasId] = nut;
            //nutBox.Nut = nut;
            //nutBox.Invalidate();
        }

        private void textureTree_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point p = new Point(e.X, e.Y);

                var node = textureTree.GetNodeAt(p);

                if (node != null)
                {
                    bool replacestate = false;

                    if (node.Level == 0)
                        replacestate = true;

                    replaceToolStripMenuItem.Enabled = replacestate;
                    textureTree.SelectedNode = node;
                    textureTreeContextMenu.Show(textureTree, p);
                }
            }
        }
    }
}
