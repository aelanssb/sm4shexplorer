﻿using System;
using System.Windows.Forms;
using Sm4shFileExplorer.Objects;
using WeifenLuo.WinFormsUI.Docking;

namespace UIBuilder
{
    public partial class FiletreePanel : DockContent
    {
        public FiletreePanel()
        {
            InitializeComponent();

            treeView1.NodeMouseDoubleClick += treeView1_NodeMouseDoubleClick;

            var resources = Plugin.Project.GetResources("data/ui/lumen/");
            var lumenRoot = resources[0];

            foreach (var res in lumenRoot.Nodes)
            {
                AddResourceToTree(treeView1.Nodes, res);
            }
        }

        private void AddResourceToTree (TreeNodeCollection parent, ResourceItem res)
        {
            TreeNode treeNode = new TreeNode();
            treeNode.Text = res.Filename.Replace('/', '\0');
            treeNode.Tag = res.AbsolutePath;

            if (Plugin.WorkspaceContains(res.AbsolutePath))
            {
                treeNode.ForeColor = System.Drawing.Color.ForestGreen;
            }
            else if (res.Source == FileSource.Patch)
            {
                treeNode.ForeColor = System.Drawing.Color.Blue;
            }

            parent.Add(treeNode);

            foreach (var resNode in res.Nodes)
            {
                AddResourceToTree(treeNode.Nodes, resNode);
            }
        }

        private void treeView1_NodeMouseDoubleClick(object sender, EventArgs e)
        {
            var filename = (string)treeView1.SelectedNode.Tag;
            if (filename != null && Plugin.IsFileSupported(filename))
                MainForm.AddEditor(Plugin.GetAsset(filename));
        }
    }
}
