﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Collections.Specialized;

namespace UIBuilder
{
    public partial class CSSEditorWindow : BaseWindow
    {
        Lumen chara_lm;

        List<Nut> chr_10Textures = new List<Nut>();
        Nut chr_10Mii;
        Nut chr_10Random;
        Nut missingImage;
        CharacterData characterData = new CharacterData();

        bool dirty = false;
        string currentFilename = null;
        ObservableCollection<int> selectedIcons = new ObservableCollection<int>();
        ObservableCollection<UndoState> undoStack = new ObservableCollection<UndoState>();
        List<UndoState> redoStack = new List<UndoState>();
        int currentLayout = 44;
        Vector2 viewPosition = new Vector2();

        bool dragging = false;
        Vector2 dragStartPosition = new Vector2();
        bool shiftHeld = false;
        bool rmbHeld = false;
        bool boxSelecting = false;
        Vector2 boxSelectStartPosition = new Vector2();
        Vector2 mousePosition = new Vector2();

        public ToolStripZoomControl statusbarZoom = new ToolStripZoomControl();
        public ToolStripComboBoxControl comboBoxLayout = new ToolStripComboBoxControl();

        public CSSEditorWindow(string filename)
        {
            InitializeComponent();

            characterData.Load();

            statusStrip1.Items.Add(new ToolStripLabel("Zoom:"));
            statusbarZoom.SelectedValueChanged += statusbarZoom_SelectedValueChanged;
            statusStrip1.Items.Add(statusbarZoom);

            statusStrip1.Items.Add(new ToolStripSeparator());
            statusStrip1.Items.Add(new ToolStripLabel("Layout:"));

            comboBoxLayout.SelectedIndexChanged += comboBoxLayout_SelectedIndexChanged;
            comboBoxLayout.ComboBoxControl.DropDownStyle = ComboBoxStyle.DropDownList;
            statusStrip1.Items.Add(comboBoxLayout);

            glControl.PreviewKeyDown += _PreviewKeyDown;
            glControl.MouseWheel += glControl_MouseWheel;

            undoStack.CollectionChanged += undoStackChanged;
            selectedIcons.CollectionChanged += selectedIconsChanged;

            currentFilename = filename;
            Text = Path.GetFileName(filename);
            TabText = Text;
        }

        protected override bool UseSave => true;
        protected override bool UseUndo => true;
        public override ToolStrip Toolbar => toolStrip1;

        private void selectedIconsChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            toolbarSwap.Enabled = (selectedIcons.Count == 2);

            bool alignEnabled = (selectedIcons.Count >= 2);
            toolbarAlignTop.Enabled = alignEnabled;
            toolbarAlignBottom.Enabled = alignEnabled;
            toolbarAlignLeft.Enabled = alignEnabled;
            toolbarAlignRight.Enabled = alignEnabled;
            toolbarAlignHorizontalCenter.Enabled = alignEnabled;
            toolbarAlignVerticalCenter.Enabled = alignEnabled;

            bool distributeEnabled = (selectedIcons.Count > 2);
            toolbarDistributeHorizonal.Enabled = distributeEnabled;
            toolbarDistributeVertical.Enabled = distributeEnabled;
        }


        private void PreviewWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (dirty)
            {
                var res = MessageBox.Show(
                    $"Save changes to {Text}?",
                    "UI Builder",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.None,
                    MessageBoxDefaultButton.Button1
                );

                if (res == DialogResult.Yes)
                {
                    Save();
                }
                else if (res == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }
            }

            Application.Idle -= Application_Idle;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!DesignMode)
            {
                Plugin.Context.MakeCurrent(glControl.WindowInfo);

                chara_lm = new Lumen(currentFilename);
                selectedIcons.Clear();

                LoadAtlasTextures();
                LoadChr_10();
                currentLayout = chr_10Textures.Count - 13; // what

                comboBoxLayout.Items.Clear();
                foreach (var label in chara_lm.movieclips[73].labels)
                {
                    comboBoxLayout.Items.Add(chara_lm.symbols[label.nameId]);
                }
                comboBoxLayout.SelectedIndex = currentLayout;

                GL.ClearColor(Color.Black);
                GL.Enable(EnableCap.Texture2D);
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

                statusbarZoom.Value = (float)glControl.ClientRectangle.Width / 1920.0f;

                Application.Idle += Application_Idle;
            }

        }

        Lumen.MovieClip getMCFromId(int id)
        {
            foreach (var mc in chara_lm.movieclips)
            {
                if (mc.id == id)
                    return mc;
            }

            return null;
        }

        Vector2 getSizeFromFaceObjectId(int oid)
        {
            var faceMc = chara_lm.objects[oid] as Lumen.MovieClip;
            int transformId = faceMc.frames[0].placements[0].positionId;

            var size = new Vector2(95, 63);
            size.X *= chara_lm.transforms[transformId].M11;
            size.Y *= chara_lm.transforms[transformId].M22;

            return size;
        }

        private void Application_Idle(object sender, EventArgs e)
        {
            if (glControl.IsIdle)
            {
                Plugin.Context.MakeCurrent(glControl.WindowInfo);
                GL.PushAttrib(AttribMask.AllAttribBits);

                GL.Clear(ClearBufferMask.ColorBufferBit);
                GL.Color3(Color.White);

                GL.MatrixMode(MatrixMode.Projection);
                GL.PushMatrix();
                GL.LoadIdentity();
                GL.Ortho(
                    0,
                    glControl.ClientRectangle.Width / statusbarZoom.Value,
                    glControl.ClientRectangle.Height / statusbarZoom.Value,
                    0,
                    0, 10
                );
                GL.Viewport(glControl.ClientRectangle);

                GL.MatrixMode(MatrixMode.Modelview);
                GL.PushMatrix();
                GL.LoadIdentity();

                GL.PushMatrix();
                GL.Translate(viewPosition.X, viewPosition.Y, 0);

                #region Draw Background
                GL.PushMatrix();
                translateToPosition(0x1261);
                //GL.BindTexture(TextureTarget.Texture2D, Plugin.TextureAtlases[chara_lm.shapes[0].graphics[0].atlasId].glId);

                GL.Begin(PrimitiveType.Quads);
                foreach (var vert in chara_lm.shapes[0].graphics[0].verts)
                {
                    //GL.TexCoord2(vert.uv.X, vert.uv.Y);
                    //GL.Vertex2(vert.pos.X, vert.pos.Y);
                }
                GL.End();
                GL.PopMatrix();
                #endregion

                var mc = chara_lm.movieclips[73];
                var oid = mc.keyframes[currentLayout].placements[0].objectId;
                Vector2 size = getSizeFromFaceObjectId(oid);

                GL.PushMatrix();
                translateToPosition(0x1262);

                for (int i = 0; i < mc.keyframes[currentLayout].placements.Count; i++)
                {
                    GL.PushMatrix();
                    var placement = mc.keyframes[currentLayout].placements[i];
                    var pos = chara_lm.positions[placement.positionId];
                    GL.Translate(pos.X, pos.Y, 0);

                    int diff = (currentLayout + 13) - chr_10Textures.Count;
                    int id = (chr_10Textures.Count - 1) - i;
                    id += diff;

                    if (MainForm.MenuViewDrawIconBackground.Checked)
                    {
                        //GL.BindTexture(TextureTarget.Texture2D, Plugin.TextureAtlases[chara_lm.shapes[0].graphics[0].atlasId].glId);
                        GL.Begin(PrimitiveType.Quads);
                        GL.TexCoord2(0.375977, 0.832031);
                        GL.Vertex2(-size.X, -size.Y);
                        GL.TexCoord2(0.375977, 0.708984);
                        GL.Vertex2(size.X, -size.Y);
                        GL.TexCoord2(0.561523, 0.708984);
                        GL.Vertex2(size.X, size.Y);
                        GL.TexCoord2(0.561523, 0.832031);
                        GL.Vertex2(-size.X, size.Y);
                        GL.End();
                    }

                    if (id == currentLayout + 11 && MainForm.MenuViewShowMiis.Checked)
                        GL.BindTexture(TextureTarget.Texture2D, chr_10Mii.glId);
                    else if (id == currentLayout + 12)
                        GL.BindTexture(TextureTarget.Texture2D, chr_10Random.glId);
                    else if (id >= 0 && id < chr_10Textures.Count && chr_10Textures[id] != null)
                        GL.BindTexture(TextureTarget.Texture2D, chr_10Textures[id].glId);
                    else
                        GL.BindTexture(TextureTarget.Texture2D, missingImage.glId);

                    GL.Begin(PrimitiveType.Quads);
                    GL.TexCoord2(0.00390625f, 0.0078125f);
                    GL.Vertex2(-size.X, -size.Y);
                    GL.TexCoord2(0.996094f, 0.0078125f);
                    GL.Vertex2(size.X, -size.Y);
                    GL.TexCoord2(0.996094f, 0.992188);
                    GL.Vertex2(size.X, size.Y);
                    GL.TexCoord2(0.00390625f, 0.992188);
                    GL.Vertex2(-size.X, size.Y);
                    GL.End();

                    if (selectedIcons.Contains(i))
                    {
                        GL.BindTexture(TextureTarget.Texture2D, 0);
                        GL.Color3(Color.Red);
                        GL.LineWidth(1);
                        GL.Begin(PrimitiveType.LineLoop);
                        GL.TexCoord2(0.00390625f, 0.0078125f);
                        GL.Vertex2(-size.X, -size.Y);
                        GL.TexCoord2(0.996094f, 0.0078125f);
                        GL.Vertex2(size.X, -size.Y);
                        GL.TexCoord2(0.996094f, 0.992188);
                        GL.Vertex2(size.X, size.Y);
                        GL.TexCoord2(0.00390625f, 0.992188);
                        GL.Vertex2(-size.X, size.Y);
                        GL.End();
                        GL.Color3(Color.White);
                    }

                    GL.PopMatrix(); // icon position
                }

                if (dragging)
                {
                    GL.LineWidth(1);
                    foreach (var selected in selectedIcons)
                    {
                        GL.PushMatrix();
                        var placement = mc.keyframes[currentLayout].placements[selected];
                        var pos = chara_lm.positions[placement.positionId];
                        GL.Translate(pos.X, pos.Y, 0);

                        var delta = mousePosition - dragStartPosition;
                        delta /= statusbarZoom.Value;
                        GL.Translate(delta.X, delta.Y, 0);

                        GL.Color3(Color.White);
                        GL.Begin(PrimitiveType.LineLoop);
                        GL.Vertex2(-size.X, -size.Y);
                        GL.Vertex2(size.X, -size.Y);
                        GL.Vertex2(size.X, size.Y);
                        GL.Vertex2(-size.X, size.Y);
                        GL.End();
                        GL.PopMatrix();
                    }
                }

                GL.PopMatrix(); // icons center
                GL.PopMatrix(); // viewOffset
            }

            if (boxSelecting)
            {
                GL.Color3(Color.Red);

                GL.Scale(1.0 / statusbarZoom.Value, 1.0 / statusbarZoom.Value, 0);
                GL.Begin(PrimitiveType.LineLoop);
                GL.Vertex2(boxSelectStartPosition.X, boxSelectStartPosition.Y);
                GL.Vertex2(mousePosition.X, boxSelectStartPosition.Y);
                GL.Vertex2(mousePosition.X, mousePosition.Y);
                GL.Vertex2(boxSelectStartPosition.X, mousePosition.Y);
                GL.End();
            }

            GL.PopMatrix();
            GL.MatrixMode(MatrixMode.Projection);
            GL.PopMatrix();
            GL.MatrixMode(MatrixMode.Modelview);
            GL.PopAttrib();

            glControl.SwapBuffers();
        }

        void moveSelectedBy(float x, float y)
        {
            if (x == 0 && y == 0)
                return;

            var mc = chara_lm.movieclips[73];

            var state = new UndoState();
            if (selectedIcons.Count > 1)
                state.text = $"move {selectedIcons.Count} icons";
            else
                state.text = "move icon";

            foreach (var selected in selectedIcons)
            {
                var positionId = mc.keyframes[currentLayout].placements[selected].positionId;
                var pos = chara_lm.positions[positionId];
                state.positions[positionId] = pos;
                chara_lm.positions[positionId] = new Vector2(pos.X+x, pos.Y+y);
            }

            redoStack.Clear();
            undoStack.Add(state);
        }

        private void glControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                selectedIcons.Clear();

            if (e.KeyCode == Keys.ShiftKey)
                shiftHeld = true;

            #region Hotkeys
            if (e.Control)
            {
                if (e.KeyCode == Keys.S)
                {
                    Save();
                }

                if (e.KeyCode == Keys.A)
                {
                    selectedIcons.Clear();

                    var mc = chara_lm.movieclips[73];
                    for (int i = 0; i < mc.keyframes[currentLayout].placements.Count; i++)
                    {
                        selectedIcons.Add(i);
                    }
                }

                if (e.KeyCode == Keys.Z)
                {
                    Undo();
                }

                if (e.KeyCode == Keys.Y)
                {
                    Redo();
                }
            }
            #endregion

            #region Item Nudging
            int delta = 1;

            if (e.Shift)
            {
                delta = 10;
            }

            if (e.KeyCode == Keys.Up)
            {
                moveSelectedBy(0, -delta);
            }

            if (e.KeyCode == Keys.Down)
            {
                moveSelectedBy(0, delta);
            }

            if (e.KeyCode == Keys.Left)
            {
                moveSelectedBy(-delta, 0);
            }

            if (e.KeyCode == Keys.Right)
            {
                moveSelectedBy(delta, 0);
            }
            #endregion
        }

        private void glControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                rmbHeld = true;
                return;
            }

            // oh god
            Vector2 mp = new Vector2(e.X, e.Y);
            mp -= viewPosition * statusbarZoom.Value;

            var mc = chara_lm.movieclips[73];
            var oid = mc.keyframes[currentLayout].placements[0].objectId;
            Vector2 size = getSizeFromFaceObjectId(oid);
            size *= statusbarZoom.Value;

            var position = chara_lm.positions[0x1262];

            // Reverse sort so selection order is same as drawing order.
            for (int i = mc.keyframes[currentLayout].placements.Count - 1; i >= 0; i--)
            {
                var placement = mc.keyframes[currentLayout].placements[i];
                var shapePos = chara_lm.positions[placement.positionId];

                // TODO: the inverse of this shit should be done to the cursor instead...
                RectangleF shape = new RectangleF(
                    (position.X * statusbarZoom.Value + shapePos.X * statusbarZoom.Value - size.X),
                    (position.Y * statusbarZoom.Value + shapePos.Y * statusbarZoom.Value - size.Y),
                    size.X * 2,
                    size.Y * 2
                );

                if (shape.Contains(mp.X, mp.Y))
                {
                    dragging = true;
                    dragStartPosition.X = e.X;
                    dragStartPosition.Y = e.Y;

                    if (selectedIcons.Contains(i))
                        return;

                    if (!shiftHeld)
                        selectedIcons.Clear();

                    selectedIcons.Add(i);

                    return;
                }
            }

            if (!shiftHeld)
            {
                selectedIcons.Clear();
            }

            boxSelecting = true;
            boxSelectStartPosition = new Vector2(e.X, e.Y);
        }

        void translateToPosition(int positionId)
        {
            var position = chara_lm.positions[positionId];
            GL.Translate(position.X, position.Y, 0);
        }

        public override void Save()
        {
            dirty = false;
            TabText = Text;

            DoStuff();

            using (var stream = new FileStream(currentFilename, FileMode.Create))
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(chara_lm.Rebuild());
            }
        }

        Nut LoadNut(string filename)
        {
            var path = Plugin.GetAsset(filename);

            if (path == null)
            {
                return null;
            }

            return new Nut(path);
        }

        void LoadChr_10()
        {
            if (chr_10Textures.Count > 0)
            {
                return;
            }

            foreach (var character in characterData.entries)
            {
                string path = "data/ui/replace/";

                if (character.isDLC == 1)
                    path = Path.Combine(path, "append/chr/chr_10/");
                else
                    path = Path.Combine(path, "chr/chr_10/");

                string name;

                if (character.characterId != -1)
                    name = characterData.names[character.characterId];
                else
                {
                    chr_10Textures.Add(null);
                    continue;
                }

                string textureFilename = Path.Combine(path, $"chr_10_{name}_01.nut");

                chr_10Textures.Add(LoadNut(textureFilename));
            }

            chr_10Mii = LoadNut("data/ui/replace/chr/chr_10/chr_10_Miiall_01.nut");
            chr_10Random = LoadNut("data/ui/replace/chr/chr_10/chr_10_Omakase_01.nut");
            missingImage = LoadNut("data/ui/replace/dummy.nut");
        }

        void LoadAtlasTextures()
        {
            foreach (var atlas in chara_lm.textureAtlases)
            {
                //Plugin.TextureAtlases.Add(LoadNut($"data/ui/lumen/chara/img-{atlas.id:d5}.nut"));
            }
        }

        void DoStuff()
        {
            //#region Toggles
            ////bool useIconBackground = true;
            //#endregion

            //bool fileModded = false;
            //int metadataColorId = -1;

            //const short magic1 = 0x4255;
            //const short magic2 = 0x5454;
            //const short configSize = 1; // in color entries
            //const short reserved = 0;

            //for (int i = 0; i < chara_lm.colors.Count; i++)
            //{
            //    if (chara_lm.colors[i].r == magic1 && chara_lm.colors[i].g == magic2)
            //    {
            //        metadataColorId = i;
            //        fileModded = true;
            //        break;
            //    }
            //}

            //if (fileModded)
            //{
            //    chara_lm.colors[metadataColorId + 1].r = Plugin.VersionMajor;
            //    chara_lm.colors[metadataColorId + 1].g = Plugin.VersionMinor;
            //    chara_lm.colors[metadataColorId + 1].b = Plugin.VersionPatch;
            //    chara_lm.colors[metadataColorId + 1].a = Plugin.VersionFlag;
            //}
            //else
            //{
            //    metadataColorId = chara_lm.colors.Count;
            //    chara_lm.colors.Add(new Lumen.Color(magic1, magic2, configSize, reserved));
            //    chara_lm.colors.Add(new Lumen.Color(
            //        Plugin.VersionMajor,
            //        Plugin.VersionMinor,
            //        Plugin.VersionPatch,
            //        Plugin.VersionFlag
            //    ));

            //    #region HD Background
            //    //var mainAtlas = chara_lm.textureAtlases[0];
            //    //mainAtlas.width = 2048.0f;
            //    //mainAtlas.height = 2048.0f;
            //    //chara_lm.textureAtlases[0] = mainAtlas;

            //    //var bgShape = chara_lm.shapes[0];
            //    //bgShape.graphics[0].verts[0] = new Lumen.Vertex(-960, 540, 0.000976563f, 0.527344f);
            //    //bgShape.graphics[0].verts[1] = new Lumen.Vertex(-960, -540, 0.000976563f, 0.000976563f);
            //    //bgShape.graphics[0].verts[2] = new Lumen.Vertex(960, -540, 0.9375f, 0.000976563f);
            //    //bgShape.graphics[0].verts[3] = new Lumen.Vertex(960, 540, 0.9375f, 0.527344f);

            //    //for (int shapeId = 1; shapeId < chara_lm.shapes.Count; shapeId++)
            //    //{
            //    //    foreach (var graphic in chara_lm.shapes[shapeId].graphics)
            //    //    {
            //    //        if (graphic.atlasId != 0)
            //    //        {
            //    //            continue;
            //    //        }

            //    //        foreach (var vert in graphic.verts)
            //    //        {
            //    //            vert.u /= 2.0f;
            //    //            vert.v = ((vert.v * 1024) + 503) / 2048.0f;
            //    //        }
            //    //    }
            //    //}
            //    #endregion
            //}

            //#region for later
            ////if (useIconBackground)
            ////{
            ////    var iconBgShape = chara_lm.shapes[103];
            ////    float w = 95.0f;
            ////    float h = 63.0f;

            ////    iconBgShape.graphics[0].verts[0] = new Lumen.Vertex(-w, h, 0.288574f, 0.726074f);
            ////    iconBgShape.graphics[0].verts[1] = new Lumen.Vertex(-w, -h, 0.288574f, 0.664551f);
            ////    iconBgShape.graphics[0].verts[2] = new Lumen.Vertex(w, -h, 0.380859f, 0.664551f);
            ////    iconBgShape.graphics[0].verts[3] = new Lumen.Vertex(w, h, 0.380859f, 0.726074f);
            ////}

            ////const int flameColorIdStart = 0xD8;
            ////const int flameColorIdStartCustom = 0xDD;

            //////chara_lm.colors[flameColorIdStart + 0] = new Lumen.Color(0x7B0A0AFF);
            //////lm.colors[flameColorIdStart + 0] = new Lumen.Color(143, 204, 215, 256);
            //////lm.colors[flameColorIdStart + 1] = new Lumen.Color(105, 225, 153, 256);
            //////lm.colors[flameColorIdStart + 2] = new Lumen.Color(225, 215, 105, 256);
            //////lm.colors[flameColorIdStart + 3] = new Lumen.Color(106, 186, 223, 256);
            //////lm.colors[flameColorIdStart + 4] = new Lumen.Color(192, 74, 74, 256);

            //////lm.colors[flameColorIdStartCustom + 0] = new Lumen.Color(0x7B0A0AFF);

            ////int[] faceMcIds = { 36, 40, 44, 48, 52, 56, 60, 64, 68, 72 };

            ////const int coverSymbolId = 0x168;

            ////int mcId = 0;
            ////foreach (var mc in chara_lm.movieclips)
            ////{
            ////    foreach (var frame in mc.frames)
            ////    {
            ////        for (int i = 0; i < frame.placements.Count; i++)
            ////        {
            ////            var placement = frame.placements[i];

            ////            // Cover mc -> graphic.
            ////            if (useIconBackground)
            ////            {
            ////                if (placement.nameId == coverSymbolId)
            ////                {
            ////                    placement.colorId1 = 0x64;
            ////                    placement.colorId2 = 0x01;
            ////                    placement.objectId = 0xE8;
            ////                }

            ////                if (faceMcIds.Contains(mcId) && placement.mcObjectId == 0x00)
            ////                {
            ////                    placement.colorId1 = 0x64;
            ////                }
            ////            }

            ////            frame.placements[i] = placement;
            ////        }
            ////    }

            ////    foreach (var frame in mc.keyframes)
            ////    {
            ////        for (int i = 0; i < frame.placements.Count; i++)
            ////        {
            ////            var placement = frame.placements[i];

            ////            if (useIconBackground && placement.nameId == coverSymbolId)
            ////            {
            ////                placement.colorId1 = 0x64;
            ////                placement.colorId2 = 0x01;
            ////                placement.objectId = 0xE8;
            ////            }

            ////            frame.placements[i] = placement;
            ////        }
            ////    }

            ////    mcId++;
            ////}
            //#endregion
        }

        private void _PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Right:
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Shift | Keys.Right:
                case Keys.Shift | Keys.Left:
                case Keys.Shift | Keys.Up:
                case Keys.Shift | Keys.Down:
                case Keys.ShiftKey:
                    e.IsInputKey = true;
                    break;
            }
        }

        private void glControl_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey)
            {
                shiftHeld = false;
            }
        }

        private void glControl_MouseMove(object sender, MouseEventArgs e)
        {
            statusbarMousePosition.Text = $"{e.X / statusbarZoom.Value - viewPosition.X:f0}, {e.Y / statusbarZoom.Value - viewPosition.Y:f0}";
            var newMousePos = new Vector2(e.X, e.Y);

            if (rmbHeld)
            {
                viewPosition += (newMousePos - mousePosition);
            }

            mousePosition = newMousePos;
        }

        private void glControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (dragging && selectedIcons.Count > 0)
            {
                var delta = mousePosition - dragStartPosition;
                delta /= statusbarZoom.Value;
                moveSelectedBy(delta.X, delta.Y);
            }
            dragging = false;

            if (e.Button == MouseButtons.Right)
            {
                rmbHeld = false;
            }

            if (boxSelecting)
            {
                boxSelecting = false;
                if (!shiftHeld)
                {
                    selectedIcons.Clear();
                }

                var mc = chara_lm.movieclips[73];
                var oid = mc.keyframes[currentLayout].placements[0].objectId;
                var position = chara_lm.positions[0x1262];
                Vector2 size = getSizeFromFaceObjectId(oid);
                size *= statusbarZoom.Value;

                // TODO: apply inverse scale to mouse coord to save computation.

                RectangleF selectionRect = new RectangleF(
                    Math.Min(boxSelectStartPosition.X, e.X) - viewPosition.X * statusbarZoom.Value,
                    Math.Min(boxSelectStartPosition.Y, e.Y) - viewPosition.Y * statusbarZoom.Value,
                    Math.Abs(e.X - boxSelectStartPosition.X),
                    Math.Abs(e.Y - boxSelectStartPosition.Y)
                );

                for (int i = mc.keyframes[currentLayout].placements.Count - 1; i >= 0; i--)
                {
                    var placement = mc.keyframes[currentLayout].placements[i];
                    var shapePos = chara_lm.positions[placement.positionId];

                    RectangleF shape = new RectangleF(
                        (position.X * statusbarZoom.Value + shapePos.X * statusbarZoom.Value - size.X),
                        (position.Y * statusbarZoom.Value + shapePos.Y * statusbarZoom.Value - size.Y),
                        size.X * 2,
                        size.Y * 2
                    );

                    if (selectionRect.IntersectsWith(shape))
                    {
                        selectedIcons.Add(i);
                    }
                }
            }
        }

        private void glControl_MouseWheel(object sender, MouseEventArgs e)
        {
            float oldscale = statusbarZoom.Value;
            float scale = oldscale + ((float)e.Delta) / 2500.0f;
            scale = Math.Min(Math.Max(scale, 0.01f), 1.0f);

            if (scale != oldscale)
            {
                viewPosition.X += (glControl.DisplayRectangle.Width / 2 - mousePosition.X) * 0.05f;
                viewPosition.Y += (glControl.DisplayRectangle.Height / 2 - mousePosition.Y) * 0.05f;
            }

            statusbarZoom.Value = scale;
        }

        private void statusbarZoom_SelectedValueChanged(object sender, EventArgs e)
        {
            if ((string)statusbarZoom.ComboBoxControl.SelectedItem == "Fit in Window")
            {
                statusbarZoom.Value = (float)glControl.ClientRectangle.Width / 1920.0f;
            }
        }

        private void comboBoxLayout_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentLayout = comboBoxLayout.SelectedIndex;
            undoStack.Clear();
            redoStack.Clear();
            selectedIcons.Clear();
        }

        public void SwapSelection()
        {
            if (selectedIcons.Count != 2)
                return;

            var mc = chara_lm.movieclips[73];
            UndoState state = new UndoState();
            state.text = "Swap Icons";

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;
            }

            chara_lm.positions[state.positions.First().Key] = state.positions.Last().Value;
            chara_lm.positions[state.positions.Last().Key] = state.positions.First().Value;

            undoStack.Add(state);
        }

        #region Alignment Tools
        public void AlignTop()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            var mc = chara_lm.movieclips[73];
            float min = float.MaxValue;
            UndoState state = new UndoState();
            state.text = "Align Top";

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;

                if (pos.Y < min)
                {
                    min = pos.Y;
                }
            }

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                chara_lm.positions[placement.positionId] = new Vector2(pos.X, min);
            }

            undoStack.Add(state);
        }

        public void AlignBottom()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            var mc = chara_lm.movieclips[73];
            float max = float.MinValue;
            UndoState state = new UndoState();
            state.text = "Align Bottom";

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;

                if (pos.Y > max)
                {
                    max = pos.Y;
                }
            }

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                chara_lm.positions[placement.positionId] = new Vector2(pos.X, max);
            }

            undoStack.Add(state);
        }

        public void AlignLeft()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            var mc = chara_lm.movieclips[73];
            float min = float.MaxValue;
            UndoState state = new UndoState();
            state.text = "Align Left";

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;

                if (pos.X < min)
                {
                    min = pos.X;
                }
            }

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                chara_lm.positions[placement.positionId] = new Vector2(min, pos.Y);
            }

            undoStack.Add(state);
        }

        public void AlignRight()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            var mc = chara_lm.movieclips[73];
            float max = float.MinValue;
            UndoState state = new UndoState();
            state.text = "Align Right";

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;

                if (pos.X > max)
                {
                    max = pos.X;
                }
            }

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                chara_lm.positions[placement.positionId] = new Vector2(max, pos.Y);
            }

            undoStack.Add(state);
        }

        public void AlignVerticalCenter()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            var mc = chara_lm.movieclips[73];
            float avg = 0;
            UndoState state = new UndoState();
            state.text = "Align Vertical Center";

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;
                avg += pos.Y;
            }

            avg /= selectedIcons.Count();

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                chara_lm.positions[placement.positionId] = new Vector2(pos.X, avg);
            }

            undoStack.Add(state);
        }

        public void AlignHorizontalCenter()
        {
            if (selectedIcons.Count == 0)
            {
                return;
            }

            var mc = chara_lm.movieclips[73];
            float avg = 0;
            UndoState state = new UndoState();
            state.text = "Align Horizontal Center";

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;
                avg += pos.X;
            }

            avg /= selectedIcons.Count();

            foreach (var sel in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[sel];
                var pos = chara_lm.positions[placement.positionId];
                chara_lm.positions[placement.positionId] = new Vector2(avg, pos.Y);
            }

            undoStack.Add(state);
        }

        public void DistributeHorizontal()
        {
            if (selectedIcons.Count == 0)
                return;

            var mc = chara_lm.movieclips[73];
            SortedDictionary<int, float> idk = new SortedDictionary<int, float>();
            UndoState state = new UndoState();
            state.text = "Distribute Horizontal";

            foreach (var iconId in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[iconId];
                var pos = chara_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;
                idk[iconId] = pos.X;
            }

            var sortedElements = idk.OrderBy(kv => kv.Value);

            float start = sortedElements.First().Value;
            float end = sortedElements.Last().Value;

            int id = 0;
            foreach (var kv in sortedElements)
            {
                var placement = mc.keyframes[currentLayout].placements[kv.Key];
                float t = ((float)id) / (sortedElements.Count() - 1);

                var pos = chara_lm.positions[placement.positionId];
                chara_lm.positions[placement.positionId] = new Vector2((1 - t) * start + t * end, pos.Y);
                id++;
            }

            undoStack.Add(state);
        }

        public void DistributeVertical()
        {
            if (selectedIcons.Count == 0)
                return;

            var mc = chara_lm.movieclips[73];
            SortedDictionary<int, float> idk = new SortedDictionary<int, float>();
            UndoState state = new UndoState();
            state.text = "Distribute Vertical";

            foreach (var iconId in selectedIcons)
            {
                var placement = mc.keyframes[currentLayout].placements[iconId];
                var pos = chara_lm.positions[placement.positionId];
                state.positions[placement.positionId] = pos;
                idk[iconId] = pos.Y;
            }

            var sortedElements = idk.OrderBy(kv => kv.Value);

            float start = sortedElements.First().Value;
            float end = sortedElements.Last().Value;

            int id = 0;
            foreach (var kv in sortedElements)
            {
                var placement = mc.keyframes[currentLayout].placements[kv.Key];
                float t = ((float)id) / (sortedElements.Count() - 1);

                var pos = chara_lm.positions[placement.positionId];
                chara_lm.positions[placement.positionId] = new Vector2(pos.X, (1 - t) * start + t * end);
                id++;
            }

            undoStack.Add(state);
        }
        #endregion

        int getTransformIdFromFaceId (int id)
        {
            var mc = chara_lm.movieclips[73];
            var oid = mc.keyframes[currentLayout].placements[id].objectId;
            var faceMc = getMCFromId(oid);
            return faceMc.frames[0].placements[0].positionId;
        }

        private void UpdateUndoState()
        {
            if (undoStack.Count == 0)
            {
                MainForm.MenuEditUndo.Text = "Undo";
                MainForm.MenuEditUndo.Enabled = false;
            }
            else
            {
                MainForm.MenuEditUndo.Text = $"Undo {undoStack.Last().text}";
                MainForm.MenuEditUndo.Enabled = true;
            }

            if (redoStack.Count == 0)
            {
                MainForm.MenuEditRedo.Text = "Redo";
                MainForm.MenuEditRedo.Enabled = false;
            }
            else
            {
                MainForm.MenuEditRedo.Text = $"Redo {redoStack.Last().text}";
                MainForm.MenuEditRedo.Enabled = true;
            }
        }

        private void undoStackChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            UpdateUndoState();

            if (args.Action != NotifyCollectionChangedAction.Reset)
            {
                dirty = true;
                TabText = Text + "*";
            }
        }

        public override void OnFocusGained()
        {
            base.OnFocusGained();
            UpdateUndoState();
        }

        public override void Undo()
        {
            if (undoStack.Count == 0)
                return;

            var state = undoStack.Last();
            var redoState = new UndoState();
            redoState.text = state.text;

            foreach (var oldPos in state.positions)
            {
                redoState.positions[oldPos.Key] = chara_lm.positions[oldPos.Key];
                chara_lm.positions[oldPos.Key] = oldPos.Value;
            }

            redoStack.Add(redoState);
            undoStack.RemoveAt(undoStack.Count - 1);
        }

        public override void Redo()
        {
            if (redoStack.Count == 0)
                return;

            var state = redoStack.Last();
            var undoState = new UndoState();
            undoState.text = state.text;

            foreach (var pos in state.positions)
            {
                undoState.positions[pos.Key] = chara_lm.positions[pos.Key];
                chara_lm.positions[pos.Key] = pos.Value;
            }

            undoStack.Add(undoState);
            redoStack.RemoveAt(redoStack.Count - 1);
        }
    }

    public class UndoState
    {
        public string text;
        public Dictionary<int, Vector2> positions = new Dictionary<int, Vector2>();

        public UndoState()
        {

        }
    }
}
