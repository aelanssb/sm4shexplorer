﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace UIBuilder
{
    public partial class ToolStripZoomControl : ToolStripControlHost
    {
        public event EventHandler ValueChanged;
        public event EventHandler SelectedValueChanged;
        private float _value;

        public ToolStripZoomControl() : base(new ComboBox())
        {
            Value = 1.0f;
            //ComboBoxControl.Items.Add("Fit in Window");
            ComboBoxControl.Items.Add("25%");
            ComboBoxControl.Items.Add("50%");
            ComboBoxControl.Items.Add("100%");
        }

        public float Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
                Text = String.Format("{0:0%}", _value);
            }
        }

        public ComboBox ComboBoxControl
        {
            get
            {
                return Control as ComboBox;
            }
        }

        private void OnValidated(object sender, EventArgs e)
        {
            try
            {
                Value = float.Parse(Text.TrimEnd(new char[] { '%', ' ' })) / 100f;

                ValueChanged?.Invoke(this, e);
            }
            catch
            {
                Text = String.Format("{0:0%}", Value);
            }
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                e.Handled = true;
                OnValidated(this, null);
            }
        }

        private void OnSelectedValueChanged(object sender, EventArgs e)
        {
            SelectedValueChanged?.Invoke(sender, e);
            OnValidated(this, null);
        }

        protected override void OnSubscribeControlEvents(Control c)
        {
            base.OnSubscribeControlEvents(c);

            ComboBox control = (ComboBox)c;

            control.SelectedValueChanged += new EventHandler(OnSelectedValueChanged);
            control.Validated += new EventHandler(OnValidated);
            control.KeyPress += new KeyPressEventHandler(OnKeyPress);
        }

        protected override void OnUnsubscribeControlEvents(Control c)
        {
            base.OnUnsubscribeControlEvents(c);

            ComboBox control = (ComboBox)c;

            control.SelectedValueChanged -= new EventHandler(OnSelectedValueChanged);
            control.Validated -= new EventHandler(OnValidated);
            control.KeyPress -= new KeyPressEventHandler(OnKeyPress);
        }
    }
}
