﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace UIBuilder
{
    public class RenderState
    {
        public Vector4 color1 = Vector4.One;
        public Vector4 color2 = Vector4.One;
        public Matrix4 transform = Matrix4.Identity;
        public int what = 1;
    };

    public struct DrawCall
    {
        public RenderState state;
        public int vbo;
        public int ibo;
        public int ati;
        public int tex;
        public int count;

        // HACK: remove me plz
        public Vector4 bounds;
    };

    public abstract class LumenObjectInstance
    {
        public RenderState renderState;
        public bool Draw = true;
    }

    public class ShapeInstance : LumenObjectInstance
    {
        public Lumen.Shape shape;

        public ShapeInstance (Lumen.Shape shape)
        {
            this.shape = shape;
        }
    }

    public class DynamicTextInstance : LumenObjectInstance
    {
        public Lumen.DynamicText text;
        public string Name = null;
        public Text textz;

        public DynamicTextInstance (Lumen.DynamicText text)
        {
            this.text = text;
        }
    }

    public class MovieClipInstance : LumenObjectInstance
    {
        public int CurrentFrame { get; set; }

        public SortedDictionary<int, LumenObjectInstance> Nodes;
        public Lumen.MovieClip mc;
        public bool Playing = true;
        public int Depth = 0;
        public string Name = null;

        public MovieClipInstance (Lumen.MovieClip mc)
        {
            this.mc = mc;
            // HACK: pls separate ticks and frames
            CurrentFrame = -1;
            Nodes = new SortedDictionary<int, LumenObjectInstance>();
        }
    }

    public class LumenDocument
    {
        public Lumen lumen;
        public MovieClipInstance Root { get; }
        public List<DrawCall> DrawList { get; }

        private Dictionary<int, Nut> textureAtlases = new Dictionary<int, Nut>();
        private Dictionary<string, int> textureNames = new Dictionary<string, int>();
        private Font fontFolk;
        private Font fontGothic;
        private Texlist texlist = null;
        private string currentFilename;

        public LumenDocument (string filename)
        {
            currentFilename = filename;
            lumen = new Lumen(filename);

            DrawList = new List<DrawCall>();

            var texlistFilename = $"data/ui/lumen/{Path.GetFileNameWithoutExtension(currentFilename)}/texlist.lst";
            texlist = new Texlist(Plugin.GetAsset(texlistFilename));
            LoadAtlasTextures();

            foreach (var tex in texlist.textures)
                textureNames[tex.name] = tex.atlasId;

            fontFolk = new Font("Folk");
            fontGothic = new Font("Gothic");

            Root = new MovieClipInstance(lumen.movieclips.Last());
            Root.renderState = new RenderState();
            Root.renderState.color1 = Vector4.One;
            Root.renderState.color2 = Vector4.One;
            Root.renderState.transform = Matrix4.Identity;
            Root.Name = "root";
        }

        public void Save (string filename)
        {
            lumen.SaveMetadata();

            using (var stream = new FileStream(filename, FileMode.Create))
            using (var writer = new BinaryWriter(stream))
                writer.Write(lumen.Rebuild());
        }

        void LoadAtlasTextures()
        {
            var name = Path.GetFileNameWithoutExtension(currentFilename);

            for (int i = 0; i < texlist.numAtlases; i++)
            {
                var path = Plugin.GetAsset($"data/ui/lumen/{name}/img-{i:d5}.nut");

                textureAtlases[i] = (path == null) ? null : new Nut(path);
            }
        }

        public void ReplaceTexture(string name, Nut nut)
        {
            textureAtlases[textureNames[name]] = nut;
        }


        public void Tick()
        {
            DrawList.Clear();
            TickBranch(Root);
        }

        private void TickBranch (MovieClipInstance parent)
        {
            if (parent.Playing)
            {
                parent.CurrentFrame++;
                parent.CurrentFrame %= parent.mc.frames.Count;
            }

            foreach (var deletion in parent.mc.frames[parent.CurrentFrame].deletions)
            {
                parent.Nodes.Remove(deletion.mcObjectId);
            }

            foreach (var action in parent.mc.frames[parent.CurrentFrame].actions)
            {
                if (action.actionId == 0)
                {
                    parent.Playing = false;
                }
            }

            foreach (var placement in parent.mc.frames[parent.CurrentFrame].placements)
            {
                var newState = new RenderState();

                if (placement.positionId == -1)
                {
                    newState.transform = parent.renderState.transform;
                }
                else if ((placement.positionFlags & 0x8000) == 0x8000)
                {
                    var pos = lumen.positions[placement.positionId];
                    newState.transform = Matrix4.CreateTranslation(pos.X, pos.Y, 0) * parent.renderState.transform;
                }
                else
                {
                    newState.transform = lumen.transforms[placement.positionId] * parent.renderState.transform;
                }

                newState.color1 = new Vector4(parent.renderState.color1);
                newState.color2 = new Vector4(parent.renderState.color2);

                //if (newState.color1.X < 0) newState.color1.X = 1;
                //if (newState.color1.Y < 0) newState.color1.Y = 1;
                //if (newState.color1.Z < 0) newState.color1.Z = 1;
                //if (newState.color1.W < 0) newState.color1.W = 1;

                //if (newState.color2.X < 0) newState.color2.X = 1;
                //if (newState.color2.Y < 0) newState.color2.Y = 1;
                //if (newState.color2.Z < 0) newState.color2.Z = 1;
                //if (newState.color2.W < 0) newState.color2.W = 1;

                if (placement.colorId1 != -1)
                {
                    var color = lumen.colors[placement.colorId1];
                    if (color.X >= 0) newState.color1.X = color.X;
                    if (color.Y >= 0) newState.color1.Y = color.Y;
                    if (color.Z >= 0) newState.color1.Z = color.Z;
                    if (color.W >= 0) newState.color1.W = color.W;
                }

                if (placement.colorId2 != -1)
                {
                    var color = lumen.colors[placement.colorId2];
                    if (color.X >= 0) newState.color2.X = color.X;
                    if (color.Y >= 0) newState.color2.Y = color.Y;
                    if (color.Z >= 0) newState.color2.Z = color.Z;
                    if (color.W >= 0) newState.color2.W = color.W;
                }

                newState.what = placement.unk3;

                //if (placement._maybeBlendMode == 2)
                //{
                //if (placement.colorId1 != -1)
                //{
                //    var color = lumen.colors[placement.colorId1];
                //    if (color.X >= 0) newState.color1.X += color.X;
                //    if (color.Y >= 0) newState.color1.Y += color.Y;
                //    if (color.Z >= 0) newState.color1.Z += color.Z;
                //    if (color.W >= 0) newState.color1.W += color.W;
                //}

                //if (placement.colorId2 != -1)
                //{
                //    var color = lumen.colors[placement.colorId2];
                //    if (color.X >= 0) newState.color2.X += color.X;
                //    if (color.Y >= 0) newState.color2.Y += color.Y;
                //    if (color.Z >= 0) newState.color2.Z += color.Z;
                //    if (color.W >= 0) newState.color2.W += color.W;
                //}
                //}
                //else
                //{
                //    if (placement.colorId1 != -1)
                //    {
                //        var color = lumen.colors[placement.colorId1];
                //        if (color.X >= 0) newState.color1.X *= color.X;
                //        if (color.Y >= 0) newState.color1.Y *= color.Y;
                //        if (color.Z >= 0) newState.color1.Z *= color.Z;
                //        if (color.W >= 0) newState.color1.W *= color.W;
                //    }

                //    if (placement.colorId2 != -1)
                //    {
                //        var color = lumen.colors[placement.colorId2];
                //        if (color.X >= 0) newState.color2.X *= color.X;
                //        if (color.Y >= 0) newState.color2.Y *= color.Y;
                //        if (color.Z >= 0) newState.color2.Z *= color.Z;
                //        if (color.W >= 0) newState.color2.W *= color.W;
                //    }
                //}

                LumenObjectInstance childInstance = null;

                if (parent.Nodes.ContainsKey(placement.mcObjectId))
                {
                    childInstance = parent.Nodes[placement.mcObjectId];
                }
                else
                {
                    var child = lumen.objects[placement.objectId];

                    if (child is Lumen.MovieClip)
                    {
                        childInstance = new MovieClipInstance(child as Lumen.MovieClip);
                        (childInstance as MovieClipInstance).Depth = parent.Depth + 1;

                        if (placement.nameId != -1)
                        {
                            (childInstance as MovieClipInstance).Name = lumen.symbols[placement.nameId];
                        }
                    }
                    else if (child is Lumen.Shape)
                        childInstance = new ShapeInstance(child as Lumen.Shape);
                    else if (child is Lumen.DynamicText)
                    {
                        childInstance = new DynamicTextInstance(child as Lumen.DynamicText);
                        (childInstance as DynamicTextInstance).Name = lumen.symbols[placement.nameId];
                        (childInstance as DynamicTextInstance).textz = new Text(fontFolk, child as Lumen.DynamicText);
                    }

                    parent.Nodes.Add(placement.mcObjectId, childInstance);
                }

                childInstance.renderState = newState;
            }

            foreach (var kv in parent.Nodes)
            {
                if (!kv.Value.Draw)
                    continue;

                if (kv.Value is MovieClipInstance)
                {
                    TickBranch(kv.Value as MovieClipInstance);
                }
                else if (kv.Value is ShapeInstance)
                {
                    var shape = (kv.Value as ShapeInstance).shape;

                    foreach (var graphic in shape.graphics)
                    {
                        var draw = new DrawCall();
                        draw.state = kv.Value.renderState;
                        draw.bounds = shape.bounds;

                        var tex = textureAtlases[graphic.atlasId];
                        draw.tex = (tex == null) ? 0 : tex.glId;

                        if (tex != null && (tex.type == PixelInternalFormat.CompressedRedRgtc1))
                            draw.ati = 1;
                        else if (tex != null && tex.type == PixelInternalFormat.CompressedRgRgtc2)
                            draw.ati = 2;
                        else
                            draw.ati = 0;

                        draw.vbo = graphic.vbo;
                        draw.ibo = graphic.ibo;
                        draw.count = graphic.indices.Length;
                        DrawList.Add(draw);
                    }
                }
                else if (kv.Value is DynamicTextInstance)
                {
                    var inst = kv.Value as DynamicTextInstance;

                    var draw = new DrawCall();
                    draw.state = kv.Value.renderState;
                    //var tex = 

                    draw.ati = 1;
                    draw.tex = fontFolk.Texture.glId;
                    draw.vbo = inst.textz.vbo;
                    draw.ibo = -1;
                    draw.count = inst.textz.numVerts;
                    DrawList.Add(draw);
                }
            }
        }
    }
}
