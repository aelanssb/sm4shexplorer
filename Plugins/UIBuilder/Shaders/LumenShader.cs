﻿using OpenTK.Graphics.OpenGL;

namespace UIBuilder
{
    public class LumenShader : Shader
    {
        protected override string vs { get; } = @"
#version 130

in vec2 aPos;
in vec2 aUV;
out vec2 vUV;

uniform mat4 uTransform;
uniform mat4 uView;

void main()
{
    vUV = aUV;
    gl_Position = uView * uTransform * vec4(aPos, 0, 1);
}
";

//        vec4 originalColor = vec4(color.rgb, 1);
//        color = vec4(1, 1, 1, 1);

//        if (originalColor.g > 0)
//        {
//            color *= color2* originalColor.gggg;
//    }
//    if (originalColor.r > 0)
//    {
//    color *= color1* originalColor.rrrr;
//}

protected override string fs { get; } = @"
#version 130

in vec2 vUV;

uniform sampler2D uTex;
uniform int uATI;
uniform vec4 uColor1;
uniform vec4 uColor2;

void main()
{
    vec4 color = texture2D(uTex, vUV);
    vec4 originalColor = vec4(color.rgb, 1);
    vec4 color1 = uColor1;
    vec4 color2 = uColor2;

    //if (uATI == 1)
    //{
    //    vec4 originalColor = vec4(color.rgb, 1);

    //    color = color1 * color.rrrr;
    //}
    //else if (uATI == 2)
    //{
    //    color = color2 * originalColor.gggg;
    //    color = mix(color, color1, originalColor.r);
    //    //color *= vec4(originalColor.gg * color2.rg, originalColor.r * color1.b, mix(originalColor.r, originalColor.g, 0.5));
    //}

    if (uATI == 1)
    {
        color = color.rrrr;
    }
    else if (uATI == 2)
    {
        color.a = color.r;
    }

    if (color.a < 0.01)
        discard;

    gl_FragColor = color;
}
";

        public int aPos { get; } = -1;
        public int aUV { get; } = -1;
        public int uTex { get; } = -1;
        public int uATI { get; } = -1;
        public int uColor1 { get; } = -1;
        public int uColor2 { get; } = -1;
        public int uTransform { get; } = -1;
        public int uView { get; } = -1;

        public LumenShader() : base()
        {
            aPos = GL.GetAttribLocation(ProgramID, "aPos");
            aUV = GL.GetAttribLocation(ProgramID, "aUV");
            uTex = GL.GetUniformLocation(ProgramID, "uTex");
            uATI = GL.GetUniformLocation(ProgramID, "uATI");
            uColor1 = GL.GetUniformLocation(ProgramID, "uColor1");
            uColor2 = GL.GetUniformLocation(ProgramID, "uColor2");
            uTransform = GL.GetUniformLocation(ProgramID, "uTransform");
            uView = GL.GetUniformLocation(ProgramID, "uView");
        }

        public override void EnableAttrib()
        {
            GL.EnableVertexAttribArray(aPos);
            GL.EnableVertexAttribArray(aUV);
            GL.EnableVertexAttribArray(uTex);
            GL.EnableVertexAttribArray(uATI);
            GL.EnableVertexAttribArray(uColor1);
            GL.EnableVertexAttribArray(uColor2);
            GL.EnableVertexAttribArray(uTransform);
            GL.EnableVertexAttribArray(uView);
        }

        public override void DisableAttrib()
        {
            GL.DisableVertexAttribArray(aPos);
            GL.DisableVertexAttribArray(aUV);
            GL.DisableVertexAttribArray(uTex);
            GL.DisableVertexAttribArray(uATI);
            GL.DisableVertexAttribArray(uColor1);
            GL.DisableVertexAttribArray(uColor2);
            GL.DisableVertexAttribArray(uTransform);
            GL.DisableVertexAttribArray(uView);
        }
    }
}
