﻿using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace UIBuilder
{
    public partial class LumenEditorWindow : BaseWindow
    {
        bool dirty = false;
        string currentFilename = null;
        Vector3 viewPosition = Vector3.Zero;

        bool rmbHeld = false;
        Vector3 mousePosition = Vector3.Zero;
        LumenShader shader = null;
        
        public LumenDocument Document = null;

        ToolStripZoomControl statusbarZoom = new ToolStripZoomControl();

        public LumenEditorWindow(string filename)
        {
            InitializeComponent();

            statusStrip1.Items.Add(new ToolStripLabel("Zoom:"));
            statusbarZoom.SelectedValueChanged += statusbarZoom_SelectedValueChanged;
            statusStrip1.Items.Add(statusbarZoom);

            glControl.PreviewKeyDown += _PreviewKeyDown;
            glControl.MouseWheel += glControl_MouseWheel;

            currentFilename = filename;
            Text = Path.GetFileName(filename);
            TabText = Text;
        }

        protected override bool UseSave => false;
        protected override bool UseUndo => false;
        public override ToolStrip Toolbar => toolStrip1;

        private void SSSEditorWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (dirty)
            {
                var res = MessageBox.Show(
                    $"Save changes to {Text}?",
                    "UI Builder",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.None,
                    MessageBoxDefaultButton.Button1
                );

                if (res == DialogResult.Yes)
                {
                    Save();
                }
                else if (res == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }
            }

            Application.Idle -= Application_Idle;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!DesignMode)
            {
                Plugin.Context.MakeCurrent(glControl.WindowInfo);

                shader = new LumenShader();

                GL.ClearColor(Color.CornflowerBlue);
                GL.Enable(EnableCap.Texture2D);
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

                Document = new LumenDocument(currentFilename);

                // TODO: use lm width.
                // TODO: use correct scale if taller than wide.
                statusbarZoom.Value = glControl.ClientRectangle.Width / 1920.0f;

                Application.Idle += Application_Idle;
            }
        }

        private void Application_Idle(object sender, EventArgs e)
        {
            while (glControl.IsIdle && MainForm.ActiveEditor == this)
            {
                Plugin.Context.MakeCurrent(glControl.WindowInfo);
                GL.PushAttrib(AttribMask.AllAttribBits);
                GL.Clear(ClearBufferMask.ColorBufferBit);

                GL.UseProgram(shader.ProgramID);
                shader.EnableAttrib();

                var ortho = Matrix4.CreateOrthographicOffCenter(
                    0, glControl.ClientRectangle.Width,
                    glControl.ClientRectangle.Height, 0,
                    0, 10
                );
                GL.Viewport(glControl.ClientRectangle);

                var view = Matrix4.CreateTranslation(viewPosition) * ortho * Matrix4.CreateScale(statusbarZoom.Value);
                GL.UniformMatrix4(shader.uView, false, ref view);

                Document.Tick();

                foreach (var draw in Document.DrawList)
                {
                    var state = draw.state;

                    GL.Uniform4(shader.uColor1, state.color1);
                    GL.Uniform4(shader.uColor2, state.color2);
                    GL.UniformMatrix4(shader.uTransform, false, ref state.transform);
                    GL.Uniform1(shader.uATI, draw.ati);

                    GL.BindTexture(TextureTarget.Texture2D, draw.tex);
                    GL.BindBuffer(BufferTarget.ArrayBuffer, draw.vbo);
                    GL.VertexAttribPointer(shader.aPos, 2, VertexAttribPointerType.Float, false, 16, 0);
                    GL.VertexAttribPointer(shader.aUV, 2, VertexAttribPointerType.Float, false, 16, 8);

                    GL.Enable(EnableCap.Blend);
                    if (state.what == 8)
                    {
                        GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
                    }
                    else
                    {
                        GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                    }

                    if (draw.ibo == -1)
                    {
                        GL.DrawArrays(PrimitiveType.Quads, 0, draw.count);
                    }
                    else
                    {
                        GL.BindBuffer(BufferTarget.ElementArrayBuffer, draw.ibo);
                        GL.DrawElements(BeginMode.Triangles, draw.count, DrawElementsType.UnsignedShort, 0);
                        GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
                    }
                }

                shader.DisableAttrib();

                glControl.SwapBuffers();

                GL.PopAttrib();
                System.Threading.Thread.Sleep(1000 / 60);
            }
        }

        public override void Save()
        {
            //dirty = false;
            //TabText = Text;

            Document.Save(currentFilename);
        }

        private void _PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Right:
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Shift | Keys.Right:
                case Keys.Shift | Keys.Left:
                case Keys.Shift | Keys.Up:
                case Keys.Shift | Keys.Down:
                case Keys.ShiftKey:
                    e.IsInputKey = true;
                    break;
            }
        }

        #region Input Events
        private void glControl_MouseMove(object sender, MouseEventArgs e)
        {
            statusbarMousePosition.Text = $"{e.X / statusbarZoom.Value - viewPosition.X:f0}, {e.Y / statusbarZoom.Value - viewPosition.Y:f0}";
            var newMousePos = new Vector3(e.X, e.Y, 0);

            if (rmbHeld)
            {
                viewPosition += (newMousePos - mousePosition) / statusbarZoom.Value;
            }

            mousePosition = newMousePos;
        }


        private void glControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                rmbHeld = true;
            }
        }

        private void glControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                rmbHeld = false;
            }
        }

        private void glControl_MouseWheel(object sender, MouseEventArgs e)
        {
            float oldscale = statusbarZoom.Value;
            float scale = oldscale + ((float)e.Delta) / 2500.0f;
            scale = Math.Min(Math.Max(scale, 0.01f), 1.0f);

            if (scale != oldscale)
            {
                viewPosition.X += (glControl.DisplayRectangle.Width / 2 - mousePosition.X) * 0.05f;
                viewPosition.Y += (glControl.DisplayRectangle.Height / 2 - mousePosition.Y) * 0.05f;
            }

            statusbarZoom.Value = scale;
        }
        #endregion

        private void statusbarZoom_SelectedValueChanged(object sender, EventArgs e)
        {
            //if ((string)statusbarZoom.ComboBoxControl.SelectedItem == "Fit in Window")
            //{
            //    statusbarZoom.Value = (float)glControl.ClientRectangle.Width / 1920.0f;
            //}
        }
    }
}
