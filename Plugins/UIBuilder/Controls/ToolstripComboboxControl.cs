﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UIBuilder
{
    public partial class ToolStripComboBoxControl : ToolStripControlHost
    {
        public event EventHandler SelectedIndexChanged;

        public ToolStripComboBoxControl() : base(new ComboBox())
        {
            // ComboBoxControl.DecimalPlaces = 2;
            // ComboBoxControl.Minimum = 1;
        }

        public ComboBox ComboBoxControl
        {
            get
            {
                return Control as ComboBox;
            }
        }

        public int SelectedIndex
        {
            get
            {
                return ComboBoxControl.SelectedIndex;
            }

            set
            {
                ComboBoxControl.SelectedIndex = value;
            }
        }

        public ComboBox.ObjectCollection Items
        {
            get
            {
                return ComboBoxControl.Items;
            }
        }

        private void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedIndexChanged?.Invoke(this, e);
        }

        protected override void OnSubscribeControlEvents(Control c)
        {
            base.OnSubscribeControlEvents(c);

            ComboBox control = (ComboBox)c;

            control.SelectedIndexChanged += new EventHandler(OnSelectedIndexChanged);
        }

        protected override void OnUnsubscribeControlEvents(Control c)
        {
            base.OnUnsubscribeControlEvents(c);

            ComboBox control = (ComboBox)c;

            control.SelectedIndexChanged -= new EventHandler(OnSelectedIndexChanged);
        }
    }
}
