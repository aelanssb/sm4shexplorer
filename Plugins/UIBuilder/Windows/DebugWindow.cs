﻿using System;
using System.Drawing;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using SharpFont;

namespace UIBuilder
{
    public partial class DebugWindow : BaseWindow
    {
        private Font font;
        private Text text;
        private LumenShader shader;

        public ToolStripZoomControl statusbarZoom = new ToolStripZoomControl();

        public DebugWindow()
        {
            InitializeComponent();

            statusStrip1.Items.Add(new ToolStripLabel("Zoom:"));
            statusStrip1.Items.Add(statusbarZoom);
        }

        int getShapeIdFromObjectId (Lumen lumen, int objectId)
        {
            for (int i = 0; i < lumen.shapes.Count; i++)
            {
                if (lumen.shapes[i].id == objectId)
                    return i;
            }

            return -1;
        }

        private void DebugWindow_Load(object sender, EventArgs e)
        {
            Plugin.Context.MakeCurrent(glControl.WindowInfo);

            GL.ClearColor(Color.Black);
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            shader = new LumenShader();
            font = new Font("Folk");

            //var library = new Library();
            //var face = new Face(library, @"D:\papyrus.ttf");
            //face.SetCharSize(Fixed26Dot6.FromDecimal(35), Fixed26Dot6.FromDecimal(55), 128, 128);
            //foreach (var fgbGlyph in font.Glyphs)
            //{
            //    face.LoadChar(fgbGlyph.Key, LoadFlags.Default, LoadTarget.Normal);
            //    face.Glyph.RenderGlyph(RenderMode.Normal);
            //    var bmp = face.Glyph.Bitmap.ToGdipBitmap();
            //    //bmp.Width
            //}

            //var lmFilenameIn = "C:/code/csseditor/Sm4shFileExplorer/bin/Debug/workspace/content/patch/data/ui/lumen/stage/stage.lm";
            var lmFilenameIn = @"C:\code\csseditor\Sm4shFileExplorer\bin\Debug\workspace\content\patch\data\ui\lumen\stage\stage.lm";
            var lmFilenameOut = "D:/s4explore/workspace/content/patch/data/ui/lumen/stage/stage.lm";
            var lumen = new Lumen(lmFilenameIn);

            //var atlas = lumen.textureAtlases[1];
            //atlas.width = 1024;
            //atlas.height = 128;
            //lumen.textureAtlases[1] = atlas;

            //{
            //    var shapeId = getShapeIdFromObjectId(lumen, 0xB2);
            //    var graphic = lumen.shapes[shapeId].graphics[0];
            //    graphic.atlasId = 1;
            //    graphic.verts[0] = new Vector4(-348, 55, 1 / 1024.0f, 109 / 128.0f);
            //    graphic.verts[1] = new Vector4(-348, -55, 1 / 1024.0f, 1 / 128.0f);
            //    graphic.verts[2] = new Vector4(348, -55, 695 / 1024.0f, 1 / 128.0f);
            //    graphic.verts[3] = new Vector4(348, 55, 695 / 1024.0f, 109 / 128.0f);
            //}

            //{
            //    var shapeId = getShapeIdFromObjectId(lumen, 0xAB);
            //    var graphic = lumen.shapes[shapeId].graphics[0];
            //    graphic.atlasId = 1;
            //    graphic.verts[0] = new Vector4(-348, 55, 1 / 1024.0f, 221 / 128.0f);
            //    graphic.verts[1] = new Vector4(-348, -55, 1 / 1024.0f,  112 / 128.0f);
            //    graphic.verts[2] = new Vector4(348, -55, 695 / 1024.0f, 112 / 128.0f);
            //    graphic.verts[3] = new Vector4(348, 55, 695 / 1024.0f, 221 / 128.0f);
            //}

            //var mc = lumen.movieclips[94];

            //foreach (var frame in mc.frames)
            //{
            //    List<Lumen.MovieClip.Placement> placementsToDelete = new List<Lumen.MovieClip.Placement>();
            //    foreach (var placement in frame.placements)
            //    {
            //        string name = lumen.symbols[placement.nameId];

            //        if (name == "effect")
            //        {
            //            placement.objectId = 0xAB;
            //            placement.nameId = -1;
            //        }

            //        if (name == "main_btn_01_bg_sel")
            //            placementsToDelete.Add(placement);

            //        if (placement.objectId == 0xB2)
            //            placementsToDelete.Add(placement);

            //        if (placement.objectId == 0xBB)
            //        {
            //            placement.objectId = 0xB2;
            //            placement.nameId = -1;
            //        }
            //    }

            //    placementsToDelete.ForEach(placement => frame.placements.Remove(placement));
            //}

            //foreach (var frame in mc.keyframes)
            //{
            //    List<Lumen.MovieClip.Placement> placementsToDelete = new List<Lumen.MovieClip.Placement>();
            //    foreach (var placement in frame.placements)
            //    {
            //        string name = lumen.symbols[placement.nameId];

            //        if (name == "effect" || placement.nameId == -1)
            //            placementsToDelete.Add(placement);
            //    }

            //    placementsToDelete.ForEach(placement => frame.placements.Remove(placement));
            //}

            //using (var stream = new FileStream(lmFilenameOut, FileMode.Create))
            //using (var writer = new BinaryWriter(stream))
            //    writer.Write(lumen.Rebuild());

            #region Footerless title
            //for (int i = 0; i < test_lm.positions.Count; i++)
            //{
            //    var pos = test_lm.positions[i];

            //    if (i != 5 && i != 9 && i != 0x1B)
            //        pos.Y += 70;

            //    test_lm.positions[i] = pos;
            //}

            //for (int i = 0; i < test_lm.transforms.Count; i++)
            //{
            //    var trans = test_lm.transforms[i];

            //    if (trans.M32 > 0)
            //        trans.M32 += 50;

            //    test_lm.transforms[i] = trans;
            //}
            #endregion

            #region HD and simplified com_bg02
            //var atlas = test_lm.textureAtlases[0];
            //atlas.width = 2048;
            //atlas.height = 2048;
            //test_lm.textureAtlases[0] = atlas;

            //atlas = test_lm.textureAtlases[1];
            //atlas.width = 1024;
            //atlas.height = 1024;
            //test_lm.textureAtlases[1] = atlas;

            //for (int shapeId = 0; shapeId < test_lm.shapes.Count; shapeId++)
            //{
            //    if (shapeId == 0)
            //    {
            //        test_lm.shapes[shapeId].graphics[0].verts[0] = new Vector4(-960, 540, 1.0f / 2048, 1079.0f / 2048);
            //        test_lm.shapes[shapeId].graphics[0].verts[1] = new Vector4(-960, -540, 1.0f / 2048, 1.0f / 2048);
            //        test_lm.shapes[shapeId].graphics[0].verts[2] = new Vector4(960, -540, 1919.0f / 2048, 1.0f / 2048);
            //        test_lm.shapes[shapeId].graphics[0].verts[3] = new Vector4(960, 540, 1919.0f / 2048, 1079.0f / 2048);
            //    }
            //    else if (test_lm.shapes[shapeId].graphics[0].atlasId == 0)
            //    {
            //        for (int vertId = 0; vertId < 4; vertId++)
            //        {
            //            var vert = test_lm.shapes[shapeId].graphics[0].verts[vertId];
            //            vert.Z = (vert.Z * 1024) / 2048;
            //            vert.W = ((vert.W * 1024) + 504) / 2048;
            //            test_lm.shapes[shapeId].graphics[0].verts[vertId] = vert;
            //        }
            //    }
            //    else if (test_lm.shapes[shapeId].graphics[0].atlasId == 1)
            //    {
            //        for (int vertId = 0; vertId < 4; vertId++)
            //        {
            //            var vert = test_lm.shapes[shapeId].graphics[0].verts[vertId];
            //            vert.Z = ((vert.Z * 1024) + 512) / 1024;
            //            test_lm.shapes[shapeId].graphics[0].verts[vertId] = vert;
            //        }
            //    }
            //    else if (test_lm.shapes[shapeId].graphics[0].atlasId == 2)
            //    {
            //        test_lm.shapes[shapeId].graphics[0].atlasId = 1;

            //        for (int vertId = 0; vertId < 4; vertId++)
            //        {
            //            var vert = test_lm.shapes[shapeId].graphics[0].verts[vertId];
            //            vert.Z = (vert.Z * 512) / 1024;
            //            test_lm.shapes[shapeId].graphics[0].verts[vertId] = vert;
            //        }
            //    }
            //}
            #endregion

            #region SSS
            #region Tab bar
            {
                string[] names = new string[] { "btn_1", "btn_2", "btn_4" };

                Action<Lumen.MovieClip.Frame> action = (Lumen.MovieClip.Frame frame) =>
                {
                    List<Lumen.MovieClip.Placement> placementsToRemove = new List<Lumen.MovieClip.Placement>();

                    foreach (var placement in frame.placements)
                    {
                        if (!names.Any(lumen.symbols[placement.nameId].Equals))
                            placementsToRemove.Add(placement);

                        if (placement.objectId == 0x1DF) // || 0x1D1?
                            placement.objectId = 0x1F2;
                    }

                    foreach (var placement in placementsToRemove)
                        frame.placements.Remove(placement);
                };

                int[] mcIds = new int[] { 176, 177, 181, 186 };
                foreach (var mcId in mcIds)
                {
                    var mc = lumen.movieclips[mcId];
                    mc.frames.ForEach(action);
                    mc.keyframes.ForEach(action);
                }

                // lower bar
                lumen.positions[0x1077] = new Vector2(960, 1020);
                // move random button
                lumen.positions[0x104A] = new Vector2(-200, 0);
            }
            #endregion
            #region Random Button
            {
                // fix text
                Action<Lumen.MovieClip.Frame> action = (Lumen.MovieClip.Frame frame) =>
                {
                    List<Lumen.MovieClip.Placement> placementsToRemove = new List<Lumen.MovieClip.Placement>();

                    foreach (var placement in frame.placements)
                    {
                        if (placement.nameId == 0x146)
                            placement.nameId = 0x13E;
                    }

                    foreach (var placement in placementsToRemove)
                        frame.placements.Remove(placement);
                };

                lumen.movieclips[178].frames.ForEach(action);
                lumen.movieclips[178].keyframes.ForEach(action);
                lumen.movieclips[184].frames.ForEach(action);
                lumen.movieclips[184].keyframes.ForEach(action);
            }
            #endregion
            #region New toggle button
            {
                // Hide active buttons
                lumen.movieclips[171].frames[16].placements.Clear(); // extra
                lumen.movieclips[171].keyframes[3].placements.Clear();
                lumen.movieclips[171].keyframes[4].placements.Clear();
                lumen.movieclips[172].frames[16].placements.Clear(); // 8-player/mymusic normal, probably
                lumen.movieclips[172].keyframes[3].placements.Clear();
                lumen.movieclips[172].keyframes[4].placements.Clear();
                lumen.movieclips[183].frames[16].placements.Clear(); // 4-player normal
                lumen.movieclips[183].keyframes[3].placements.Clear();
                lumen.movieclips[183].keyframes[4].placements.Clear();

                // make buttons positions uniform
                string[] names = new string[] { "btn_1", "btn_2" };

                Action<Lumen.MovieClip.Frame> action = (Lumen.MovieClip.Frame frame) =>
                {
                    foreach (var placement in frame.placements)
                    {
                        if (names.Any(lumen.symbols[placement.nameId].Equals))
                            placement.positionId = 0x1048;
                    }
                };

                int[] mcIds = new int[] { 176, 177, 181, 186 };
                foreach (var mcId in mcIds)
                {
                    var mc = lumen.movieclips[mcId];
                    mc.frames.ForEach(action);
                    mc.keyframes.ForEach(action);
                }

                // move button
                lumen.positions[0x1048] = new Vector2(200, 0);
            }
            #endregion
            #region Header
            {
                Action<Lumen.MovieClip.Frame> action = (Lumen.MovieClip.Frame frame) =>
                {
                    List<Lumen.MovieClip.Placement> placementsToRemove = new List<Lumen.MovieClip.Placement>();

                    foreach (var placement in frame.placements)
                    {
                        if (!lumen.symbols[placement.nameId].StartsWith("return_btn"))
                            placementsToRemove.Add(placement);
                    }

                    foreach (var placement in placementsToRemove)
                        frame.placements.Remove(placement);
                };

                lumen.movieclips[199].frames.ForEach(action);
                lumen.movieclips[199].keyframes.ForEach(action);
            }
            #endregion
            #region Preview, series, and nameplate
            {
                var mc = lumen.movieclips[27];

                // move everything up and centered
                lumen.positions[0x107A] = new Vector2(470, -420);

                // move preview down
                var xform = lumen.transforms[0x214];
                xform.M32 = 552;
                lumen.transforms[0x214] = xform;

                //// move mark to corner
                //var markXform = lumen.transforms[0x215];
                //xform.M31 = 140;
                //xform.M32 = 960;
                //lumen.transforms[0x215] = markXform;

                Action<Lumen.MovieClip.Frame> action = (Lumen.MovieClip.Frame frame) =>
                {
                    List<Lumen.MovieClip.Placement> placementsToRemove = new List<Lumen.MovieClip.Placement>();

                    foreach (var placement in frame.placements)
                    {
                        if (placement.objectId == 0x30)
                            placementsToRemove.Add(placement);
                    }

                    foreach (var placement in placementsToRemove)
                        frame.placements.Remove(placement);
                };

                lumen.movieclips[27].frames.ForEach(action);
                lumen.movieclips[27].keyframes.ForEach(action);
            }
            #endregion
            #region Hide Page Toggle
            {
                Action<Lumen.MovieClip.Frame> action = (Lumen.MovieClip.Frame frame) =>
                {
                    List<Lumen.MovieClip.Placement> placementsToRemove = new List<Lumen.MovieClip.Placement>();

                    foreach (var placement in frame.placements)
                    {
                        if (lumen.symbols[placement.nameId] == "img_99")
                            placementsToRemove.Add(placement);
                    }

                    foreach (var placement in placementsToRemove)
                        frame.placements.Remove(placement);
                };

                lumen.movieclips.ForEach((mc) =>
                {
                    mc.frames.ForEach(action);
                    mc.keyframes.ForEach(action);
                });
            }
            #endregion
            #endregion

            using (var stream = new FileStream(lmFilenameOut, FileMode.Create))
            using (var writer = new BinaryWriter(stream))
                writer.Write(lumen.Rebuild());

            text = new Text(font, @"I have a lot of old paint. 
It's all in gallon cans. 
The colors do not match. 
That's out of my hands.

Open a can of white paint. 
A thick skin's on the top. 
Break the skin and stir it, 
pick out all the glop.

The next can is of red paint. 
The lid is stuck on tight. 
Finally it's open 
It took all my might.

Why did I buy that brown paint? 
By now I do not care. 
I'll add it to the mix. 
Stop me if you dare.

I almost have a gallon, 
and there's a little more. 
Here's some teal enamel, 
add it to the pour.

The shade is not very nice. 
But color's not the point. 
Just want to use it up, 
Make space in this joint.

I paint the door, sloppily, 
I paint the dog, the car. 
I look for something else, 
There's the local bar.

I paint the stools and beer taps. 
And Hilda's pretty dress. 
Someone has a problem, 
Seems I've made a mess.

Do not paint out of control. 
take a tip from me. 
If you have extra paint, 
best to leave it be.", 14);

            Application.Idle += Application_Idle;
        }

        private void DebugWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Idle -= Application_Idle;
        }

        private void Application_Idle(object sender, EventArgs e)
        {
            if (glControl.IsIdle)
            {
                Plugin.Context.MakeCurrent(glControl.WindowInfo);

                GL.PushAttrib(AttribMask.AllAttribBits);
                GL.ClearColor(Color.CornflowerBlue);
                GL.Clear(ClearBufferMask.ColorBufferBit);

                GL.UseProgram(shader.ProgramID);
                shader.EnableAttrib();

                var ortho = Matrix4.CreateOrthographicOffCenter(
                    0,
                    glControl.ClientRectangle.Width / statusbarZoom.Value,
                    glControl.ClientRectangle.Height / statusbarZoom.Value,
                    0,
                    0, 10
                );
                var t = Matrix4.CreateTranslation(200, 100, 0);

                GL.UniformMatrix4(shader.uView, false, ref ortho);
                GL.UniformMatrix4(shader.uTransform, false, ref t);
                GL.Uniform4(shader.uColor1, Color.White);
                GL.Uniform4(shader.uColor2, Color.FromArgb(0xff, 27, 27, 27));

                text.Draw(shader);

                shader.DisableAttrib();
                glControl.SwapBuffers();
                GL.PopAttrib();
            }
        }
    }
}
