﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace UIBuilder
{
    public class Text
    {
        public string Content;
        public float size;
        private Font font;
        Lumen.DynamicText.Alignment align = Lumen.DynamicText.Alignment.Left;

        public int vbo;
        public int numVerts;

        public bool le = true;

        public Text (Font font, string text, float size)
        {
            Content = text;
            this.font = font;
            this.size = size;

            bufferText(Content);
        }

        public Text (Font font, Lumen.DynamicText text)
        {
            Content = text.placeholder;
            size = text.size;
            align = text.alignment;
            this.font = font;

            bufferText(Content);
        }

        ~Text()
        {
            //GL.DeleteBuffer(vbo);
        }

        public void Draw (LumenShader shader)
        {
            GL.BindTexture(TextureTarget.Texture2D, font.Texture.glId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.VertexAttribPointer(shader.aPos, 2, VertexAttribPointerType.Float, false, 16, 0);
            GL.VertexAttribPointer(shader.aUV, 2, VertexAttribPointerType.Float, false, 16, 8);
            GL.DrawArrays(PrimitiveType.Quads, 0, numVerts);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        private float getLineLength (string line)
        {
            float lineLength = 0;

            foreach (char c in line)
            {
                if (c == ' ')
                {
                    lineLength += font.spaceWidth;
                }
                else if (font.Glyphs.ContainsKey(c))
                {
                    Font.Glyph glyph = font.Glyphs[c];

                    lineLength += glyph.width + glyph.advance;
                }
            }

            return lineLength;
        }

        private float getTextLength (string text)
        {
            float length = 0;

            var lines = text.Split('\n');
            foreach (var line in lines)
            {
                float lineLength = getLineLength(line);

                if (lineLength > length)
                    length = lineLength;
            }

            return length;
        }

        private void bufferText (string text)
        {
            var len = getTextLength(text);

            float x = 0;
            float y = 0;

            var verts = new List<Vector4>();
            float scale = size / font.defaultSize;

            var lines = text.Split('\n');
            foreach (var line in lines)
            {
                if (align == Lumen.DynamicText.Alignment.Right)
                    x = (len - getLineLength(line)) * scale;
                else if (align == Lumen.DynamicText.Alignment.Center)
                    x = (len - getLineLength(line)) * scale / 2;
                else
                    x = 0;

                foreach (char c in line)
                {
                    if (c == ' ')
                    {
                        x += font.spaceWidth * scale;
                        continue;
                    }

                    // TODO: render box for missing glyphs?
                    if (!font.Glyphs.ContainsKey(c))
                        continue;

                    Font.Glyph glyph = font.Glyphs[c];

                    verts.Add(new Vector4(
                        x, y + glyph.yBearing * scale,
                        glyph.x / font.Texture.width, glyph.y / font.Texture.height
                    ));

                    verts.Add(new Vector4(
                        x + glyph.width * scale, y + glyph.yBearing * scale,
                        (glyph.x + glyph.width) / font.Texture.width, glyph.y / font.Texture.height
                    ));

                    verts.Add(new Vector4(
                        x + glyph.width * scale, y + (glyph.yBearing + glyph.height) * scale,
                        (glyph.x + glyph.width) / font.Texture.width, (glyph.y + glyph.height) / font.Texture.height
                    ));

                    verts.Add(new Vector4(
                        x, y + (glyph.yBearing + glyph.height) * scale,
                        glyph.x / font.Texture.width, (glyph.y + glyph.height) / font.Texture.height
                    ));

                    x += (glyph.width + glyph.advance) * scale;
                }

                y += font.lineHeight * scale;
            }

            vbo = GL.GenBuffer();
            numVerts = verts.Count;
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(numVerts * Vector4.SizeInBytes), verts.ToArray(), BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }
    }
}
